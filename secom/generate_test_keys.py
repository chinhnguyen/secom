'''
Created on Jun 20, 2014

@author: ntr9h
'''
import random, string
    
if __name__ == '__main__':
    for i in range(10):
        print '-'.join([
            ''.join(random.choice(string.ascii_uppercase) for i in range(5)),
            ''.join(random.choice(string.ascii_uppercase) for i in range(5)),
            ''.join(random.choice(string.ascii_uppercase) for i in range(5)),
        ])