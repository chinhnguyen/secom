'''
Created on Feb 19, 2014

@author: ntr9h
'''

import base64, time, random, binascii, traceback
from suds.client import Client
from Crypto.PublicKey import RSA
from Crypto.Cipher.PKCS1_v1_5 import PKCS115_Cipher
from Crypto.Hash import MD5
from Crypto.Cipher import DES3
from Crypto1.Util import Padding


# DEBUG
debug_publickey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs+JvfyTOMHqvjxHJyDZGHZpz3atV7qcOT8mijXGGG3S+8Bb2p2kREGJwrzC2IIErCQUcZ3Wa3wTugKQDxqXESPt76HN2353ufegbvTI9kYgK0MLFpY8OZAMsaTytVrvUEVHjqGXZO4z7oVTqByuBwcZAvK+sN39+MqisS6ZejACbbQLkWZgcSgt5wBAaDaEa2lvRYcVbNyO/mqTU6SSfd+w78uM07BpmxhimOMwf+l/qs+Z04LUm4Ay7b+AHHAwbaHeehC1wInzNDfipgR0H0FCa/LOnEblj2HVpptB/NY4XNG+CDHTBKkxzEw92D/Nj1JIlr1oP0l+/VdAnxxiWuQIDAQAB'
debug_privatekey = 'MIICXAIBAAKBgQCxjjvVYnRTcT70Cd68nE08DKwuMlSeOfbcgtTvgawgC/18p/Xs6wcg2U2xn0nn+9O6x+pJ9o5uQa6iqTgOJz1jVKWSwWuFD44muEyqMdeE1IYWbrJOFUFIU54Cau3qS3GKgWtOTAZUYqVh+cOPxgo0pW2is+swT6tz56/P0p22vwIDAQABAoGAaFrGQ9XHtLscWuXqKURcCG0STVx7azt6IYQrjlDST0t8wmUdHw/Lcr0E8t5B27ygZmjVBH+Kmraz4xo9vePGKb5Z/+BcRsHhkQcmRUJlvML6hl8jSartVs4g8j6qEzXsXPe4rEiSxpOSWm7tDlCB/X//FbfLOJB7kc9Xuy/PxRECQQDfEr82UpIUTPJNdL3AWSIH2KqOL3XcxjhwakQq8JdZTLDgoRHx6AQ5RvjTOeIAYDZdSM33iZeDqdTxszbKi4WLAkEAy8OD5HLTVLzRn3JMwZnTxIHdwYdRnpY/OcBLoaW2zI49hUN9YkXgkaD+JpuP7vLPOfvFvHEDhjTjW6Ms+t2CHQJABdufK9UFQwU2Q7RyGy/8Bcq5x9wVM0P9TW5s9de1kcHCz1NLflfCbKKhfCKD/dCI/PAhgIObd/iov+Qd5zm8uwJBALZDlzC7vWlo1KEpXps2fASknbXE0y9l+fwwk/ZuAsuK2GDh87/5/VyGg5AJSoBU1SRqn39mH97mZBDOLyffB8kCQDWqDp4Kw86VW1yKPl3eIT3sLlXzNq6o6PLw2sttUf0OUAzPiucS7OhMnYeSq/oqIGk/DvGsQnTgTXtDIBUCswE='
debug_partnercode = '00241'
debug_partnerid = 'thuynhan'
debug_mpin = '123456'
debug_user = 'thuynhan'
debug_pass = '123456'
debug_service_url = 'http://115.78.133.42:9090/CardChargingGW/services/Services?wsdl'
# PRODUCTION
production_publickey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs+JvfyTOMHqvjxHJyDZGHZpz3atV7qcOT8mijXGGG3S+8Bb2p2kREGJwrzC2IIErCQUcZ3Wa3wTugKQDxqXESPt76HN2353ufegbvTI9kYgK0MLFpY8OZAMsaTytVrvUEVHjqGXZO4z7oVTqByuBwcZAvK+sN39+MqisS6ZejACbbQLkWZgcSgt5wBAaDaEa2lvRYcVbNyO/mqTU6SSfd+w78uM07BpmxhimOMwf+l/qs+Z04LUm4Ay7b+AHHAwbaHeehC1wInzNDfipgR0H0FCa/LOnEblj2HVpptB/NY4XNG+CDHTBKkxzEw92D/Nj1JIlr1oP0l+/VdAnxxiWuQIDAQAB'
production_privatekey = 'MIICXgIBAAKBgQDG6mH3/0Z3499/+1HZ0a1hQxnmFVPTLxHYXQkBC2+bNJQzFLLRdUanwBETt4siIA6EuZf8Rezbo8Kql6ONtlWS3/sTp5MPxWVzqN2rsj4p0fAvB+wYc/TPDnk+LL6PcUwBsEn/s54hbqqpnFsA8l73yO7O3Qt27dGSHe8sTws1rQIDAQABAoGAFEWYvGR4swk3ItzjB5bV256yCf/Mbi5S3EEHBOM8eY3POLF4U95m1/u8Ac1n0Y+Ay0f61EvwCqZS1X3kvjGB4OsQtKkn+sN5yUgmN7YRBSnSHpI0cSZA1BR+rfp4VIJz8UU1VgYRms2o002WMBeSXiqK9Wd4oQyMQ7xl3Gm6iMECQQDk0WTeCVw1kIFV2lcB7+98vL2FLtDaGFfk9A5lZTe0WrnV0j6g0rZhjwgZ5q3aIS/AEJ/mM//Be7zx/rUm4jy9AkEA3oufxH4DnSY2quHpxyj2krVTXyGSxqszJ2vfDUmqupZMSzVvm8wJTUj5xARDPFRn1QD9AUQR+kP14RkZ/p0DsQJBAK8c3O/cA/huRbzZYtr1r7crjnLdeDJgjOuIJ3hLtQzOngfYZduYRsF0wANqecREv5FiZ6y6QNHnC5u7jRFb9JUCQQDBtluNl/rhUaAIQrlRaLCopzk492phkCfoypbPZC4+WBF07cCGpqX6Jely3BsCap4u5LtV3MChTYK7OzRLUrUhAkEAnldOdyJNkoRn8prvHv5dNwh4Tt7rMsOfAMpN0TsZsCnpeX6lO2MBRxOJbxsRrAr1kBWbIXLI4BuzxSEHhWmrzw=='
production_partnercode = '00593'
production_partnerid = 'HM190'
production_mpin = 'bvbfjoucq'
production_user = 'HM190'
production_pass = 'gmusfmoxh'
production_service_url = 'http://charging-service.megapay.net.vn/CardChargingGW_V2.0/services/Services?wsdl'


def pay(number, serial, provider, test, enable_logging):
    if not number or len(str(number).strip()) == 0 \
       or not serial or len(str(serial).strip()) == 0 \
       or not provider or len(str(provider).strip()) == 0:
        return False, 'NOK-9999'
    number = str(number)
    serial = str(serial)
    provider = str(provider)
    model = __EpayModel(test)
    
    if enable_logging == True:
        import logging
        logging.basicConfig(level=logging.INFO)
        logging.getLogger('suds.client').setLevel(logging.DEBUG)
    
    try:    
        model.login()
        return (True, model.charge(provider, number, serial))
    except:
        print traceback.format_exc()
        return False, 'NOK-9998'
    finally:
        model.logout()
    
    
    
class __EpayModel:
    def __init__(self, test):    
        if test:
            prefix = 'debug_'
        else:
            prefix = 'production_'
        self.public_key = eval(prefix + 'publickey')
        self.private_key = eval(prefix + 'privatekey')
        self.partner_code = eval(prefix + 'partnercode')
        self.partner_id = eval(prefix + 'partnerid')
        self.mpin = eval(prefix + 'mpin')
        self.user = eval(prefix + 'user')
        
        # Build the SOAP client
        self.epay_service = Client(eval(prefix + 'service_url')).service        
        # Build the cipher for RSA encrypt/decrypt
        rsa = RSA.importKey(base64.b64decode(self.public_key))
        self.public_cipher = PKCS115_Cipher(rsa)
        rsa = RSA.importKey(base64.b64decode(self.private_key))
        self.private_cipher = PKCS115_Cipher(rsa)
        # encrypt the password
        self.password = self.__encryptRSA(eval(prefix + 'pass'))
        
    def login(self):
        login_result = self.epay_service.login(self.user, self.password, self.partner_id)
        if str(login_result.status) != '1':
            raise ValueError('Bad epay gateway configuration')
        # store the session id        
        self.session_id = self.__decryptRSA(login_result.sessionid)
        md5 = MD5.new()
        md5.update(self.session_id)
        self.hex_session_id = md5.hexdigest()
        
        if len(self.session_id) != 48:
            raise ValueError('Invalid key')
        bin_key = binascii.unhexlify(self.session_id)
        self.des3 = DES3.new(bin_key)        
        
    def logout(self):
        if not hasattr(self, 'hex_session_id') or not self.hex_session_id:
            return
        try: 
            self.epay_service.logout(self.user, self.partner_id, self.hex_session_id)
        except:
            # Do not throw exception, it is not necessary
            pass
        finally:
            del self.session_id     
    
    
    def charge(self, provider, number, serial):
        if not hasattr(self, 'session_id') or not self.session_id:
            raise Exception('This operation requires a valid sessionId')
        card_data = self.__buildCardData(provider, number, serial)
        if not card_data:
            raise Exception('Bad card information %s/%s/%s' % (provider, number, serial))
        
        transaction_id = '%s_%s_%04d' % (self.partner_code, time.strftime("%d%m%Y%H%M%S"), random.randint(0, 10000))
        target = str(random.randint(0, 100000000))
        enc_mpin = self.__encryptDES3(self.mpin)
        enc_card_data = self.__encryptDES3(card_data)
        result = self.epay_service.cardCharging(transaction_id, self.user, \
                                                self.partner_id, enc_mpin, \
                                                target, enc_card_data, \
                                                self.hex_session_id)
        if not result:
            return 'NOK-9997'        
        if str(result.status) != '1':
            return 'NOK-' + result.status
        return self.__decryptDES3(str(result.responseamount))

    def __buildCardData(self, provider, number, serial):
        if provider == 'VNP' \
           or provider == 'VMS' \
           or provider == 'VTT' \
           or provider == 'VTC' \
           or provider == 'FPT':
            return '%s:%s::%s' % (serial, number, provider)
        if provider == 'MGC':
            return '%s:%s' % (number, provider)
        return None

    def __encryptRSA(self, data):
        raw_enc = self.public_cipher.encrypt(data)
        return base64.b64encode(raw_enc)
    
    def __decryptRSA(self, enc_data):
        return self.private_cipher.decrypt(base64.b64decode(enc_data), 0)

    def __encryptDES3(self, data):
        data = Padding.pad(data, 8)
        return binascii.hexlify(self.des3.encrypt(data))
    
    def __decryptDES3(self, enc_data):
        data = self.des3.decrypt(binascii.unhexlify(enc_data))
        return Padding.unpad(data, 8)    
    

if __name__ == '__main__':
    pay('12345678', '12345678', 'VNP', True, False)