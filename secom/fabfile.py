'''
Created on Jun 10, 2014

@author: ntr9h
'''
import os, posixpath
from fabric.api import env, run
from fabric import network
from fabric.utils import puts

from deploy import deploy
from deploy.system import create_system_account

env.APP_NAME = "secom"
env.SUDO_USER = "root"
env.APP_USER = "pmht"
env.PROJECT_PATH = posixpath.join(posixpath.split (__file__)[0], env.APP_NAME)
env.REMOTE_SETTINGS_TEMPLATE = 'deploy/conf/server_settings.py'
env.PIP_CONF = "%s/deploy/conf/pip.conf" % env.APP_NAME
env.NGINX_CONFIG_TEMPLATE = "deploy/conf/nginx.conf"
env.GUNICORN_BASH_TEMPLATE = 'deploy/conf/gunicorn_start.sh'
env.SUPERVISOR_CONF_TEMPLATE = 'deploy/conf/supervisor.conf'

env.DJ_ADMIN_USERNAME='pmhtadmin'
env.DJ_ADMIN_EMAIL='nguyen.trungchinh@gmail.com'
env.DJ_FIXTURES = ['faqs.json']

env.GIT_BRANCH = 'master'

staging_conf = dict(
    DEBUG=True,
    HOST="phanmemhoctap.vn",
    PORT=25610,
    SSL_PORT=25443,
    DB_NAME="pmht_stg",
    DB_USER="pmht",
    DB_PASSWORD="7n8V4e9QghT5Wmw",
)

production_conf = dict(
    DEBUG=False,
    HOST="phanmemhoctap.vn",
    PORT=80,
    SSL_PORT=443,
    DB_NAME="pmht_pro",
    DB_USER="pmht",
    DB_PASSWORD="48xsGgfvhTT5zd9"
)

def __prepare_env(config):
    env.CONFIG = config
    env.INSTANCE_NAME="%s_%s" % (env.APP_NAME, config)

    env.USER_PATH = '/home/%s' % env.APP_USER
    env.APP_PATH = posixpath.join(env.USER_PATH, config)
    env.STATIC_PATH= posixpath.join(env.APP_PATH, "static")
    env.MEDIA_PATH= posixpath.join(env.APP_PATH, "media")
    env.LOGS_PATH= posixpath.join(env.APP_PATH, "logs")
    
    exec('env.update(%s_conf)' % config)
    env.host_string = network.join_host_strings(env.APP_USER, env.HOST, 22)

def run(**kwargs):
    config = kwargs.get('config', 'staging')
    __prepare_env(config)
    deploy.task(**dict(kwargs))    

def quick_deploy(**kwargs):
    run(command='quick_deploy', **dict(kwargs))
    
def create_user(pub_key_file, config='staging'):
    __prepare_env(config)
    create_system_account(pub_key_file)
    

