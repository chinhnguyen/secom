# -*- coding: utf-8 -*-
'''
Created on May 8, 2014

@author: ntr9h
'''
from django.http import Http404, HttpResponse
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.auth import login, logout, authenticate, tokens
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import PasswordResetForm, PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.views.defaults import bad_request
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.shortcuts import render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django import forms
from captcha.fields import ReCaptchaField
from rest_framework.renderers import JSONRenderer
from secom import settings, now
from secom.templatetags import formatter
from secom.models import *
from secom.mailer import receipt
from secom.mailer import customer as customer_email
from payment import epay

import traceback, sys, random, re, itertools
 

def get_configuration():
    return Block.objects.get(name='settings').data

def get_base_context(request):
    config = get_configuration()
    
    try:
        # get the content for sliding
        hot_contents = Block.objects.get(name='slide-content').data    
        if len(hot_contents) > config['number_hot_news']:
            hot_contents = random.sample(hot_contents, 3)
    except Block.DoesNotExist:
        hot_contents = []
        
    try:
        # get the content for sliding
        top_nav = Block.objects.get(name='top-navigation').data    
    except Block.DoesNotExist:
        top_nav = None
        
    try:
        # get the content for sliding
        fb_og = Block.objects.get(name='fb-og').data    
    except Block.DoesNotExist:
        fb_og = None
        
    context = {
            'request': request,
            # top navigation
            'top_nav': top_nav,
            # sliding content
            'hot_contents': hot_contents,
            # show cart menu for almost all pages
            'enable_popup_cart': True,
            # the domain 
            'domain': "http://%s" % request.META['HTTP_HOST'],
            # facebook og meta tags
            'fb_og': fb_og
            }
    update_user_context(context, request)
    
    context.update(csrf(request))
    return context
    

def home(request):    
    config = get_configuration()
    # prepare hot products 
    hot_products = Product.objects.filter(active=True, display_on_home=True)
    if len(hot_products) > config['number_products_per_row']:
        hot_products = random.sample(hot_products, 3)
    
    # prepare the hot post
    hot_news = Post.objects.filter(active=True, display_on_home=True)
    if len(hot_news) > 0: 
        hot_new = random.sample(hot_news, 1)[0]
    else:
        hot_new = None  
    
    # prepare the latest post
    latest_news = Post.objects.order_by('created_time')
    if hot_new:
        latest_news = latest_news.exclude(id=hot_new.id)
    if len(latest_news) > config['number_hot_news']:
        latest_news = latest_news[:config['number_hot_news']]
        
    # get product types for filtering
    product_types = Block.objects.get(name='product-filters').data

    # get the base context
    c = get_base_context(request)
    # update with view's specific context
    c.update({
     'big_header': True,
     'hot_new': hot_new,
     'hot_products': hot_products,
     'latest_news': latest_news,
     'product_types': product_types
    })
    return render_to_response('secom/home.html', c)

def chunk(src, size):
    if len(src) < size:
        return [src]
    return map(None, *([iter(src)] * size))

def products(request, path=''):
    config = get_configuration()
    record_per_page = request.GET.get('rpp', config['number_products_per_page'])
    record_per_row = request.GET.get('rpr', config['number_products_per_row'])
    
    page = 1   
    if len(path) > 0:
        # split up to parts
        tags = path.split('/')
        # remove empty parts
        tags = filter(None, tags)        
        # try to get the page parameter
        if tags[-1].startswith('trang-'):
            # remove the last element and get the page value
            page = tags.pop()[6:]
    else:
        tags = []
    
    c = get_base_context(request)
    
    # get production navigation
    products_navigation = Block.objects.get(name='products-navigation').data
    # get product types for filtering
    product_types = Block.objects.get(name='product-filters').data
   
    # find the single hot-product
    hot_products = Product.objects \
        .filter(active=True, display_as_hot_product=True) \
        .order_by('?')
    hot_product = None 
    if len(hot_products) > 0:
        hot_product = hot_products[0]    
        
    # get the list of product
    products = Product.objects.filter(active=True).order_by('created_time', 'id')
    # appending the tags query condition
    if len(tags) > 0:
        products = products.filter(tags__name__in=tags)
    products = products.distinct('created_time', 'id')
    
    if products.count() > 0:
        # create the paginator
        paginator = Paginator(products, record_per_page)
         
        # get the right page
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)
        
        # handling the paging
        root = '/san-pham/danh-sach'  
        if len(tags) > 0:
            root = root + '/' + '/'.join(tags)        
        next_page = ''
        if products.has_next():
            next_page = root + '/trang-' + str(products.next_page_number())
        previous_page = ''
        if products.has_previous():
            previous_page = root + '/trang-' + str(products.previous_page_number())
            
        c.update({
          'products': products,
          'products_chunks': chunk(products, record_per_row),
          'next_page': next_page,
          'previous_page': previous_page,
        })
    else:
        # get the list of product
        products = Product.objects.filter(active=True).order_by('created_time', 'id')
        if len(products) > 3:
            products = random.sample(products, 3);
        c.update({
            'suggestions': products
        })


    # find which product-nav to be highlighted
    active_product_nav = None
    matches = filter(lambda n: n['id'] in tags, products_navigation)
    if len(matches) > 0:
        active_product_nav = matches[0]['id']
        title = matches[0]['title']
    else:
        # calculate the title
        types = list(itertools.chain(*[g['values'] for g in product_types]))
        tag_names = [t['name'] for t in types if t['value'] in types]
        if len(tag_names) > 0:
            title = u', '.join(tag_names)
        else:
            title = None
        
    c.update({
      'product_nav': products_navigation,
      'active_product_nav': active_product_nav,
      'hot_product': hot_product,
      'product_types': product_types,
      'tags': tags,
      'title': title,
    })
    return render_to_response('secom/products.html', c)

def product_preview(request, name=''):
    product = Product.objects.get(active=True, name=name)
    c = {
      'product': product
    }
    return render_to_response('secom/product_preview_page.html', c)

def product_detail(request, name=''):
    c = get_base_context(request)
    # get the product
    product = None
    try:
        product = Product.objects.get(active=True, name=name)
    except Product.DoesNotExist:
        raise Http404

    # get production navigation
    products_navigation = Block.objects.get(name='products-navigation').data
    # find which product-nav to be highlighted
    active_product_nav = None
    matches = filter(lambda n: n['id'] in product.tags.names(), products_navigation)
    if len(matches) > 0:
        active_product_nav = matches[0]
    
    # get the config    
    config = get_configuration()

    # get the list of suggestions
    suggestions = Product.objects \
      .filter(active=True, tags__name__in=product.tags.names()) \
      .exclude(id=product.id) \
      .distinct('created_time', 'id') \
      .order_by('created_time', 'id')
    if len(suggestions) > 0:
        if len(suggestions) > config['number_same_product_type_suggestion']:
            suggestions = random.sample(suggestions, config['number_same_product_type_suggestion'])
        c.update({'suggestions': suggestions})
        
    if 'trailer' in product.data:
        import urlparse
        trailer_url = urlparse.urlparse(product.data['trailer'])
        query = urlparse.parse_qs(trailer_url.query)
        if 'v' in query:
            video_id = query["v"][0]        
            c.update({"trailer": "https://youtube.com/v/%s?version=3&enablejsapi=1" % video_id })     
        
    if 'gallery' in product.data:
        images = product.data['gallery']
        if len(images) > 0:
            c.update({"images": images})
            
    if product.is_combo():
        c.update({
                  "subtotal": product.sub_total(),
                  "saving": product.price() - product.sub_total()})
        
    
    # get product types for filtering
    product_types = Block.objects.get(name='product-filters').data

    c.update({        
      'product_nav': products_navigation,
      'active_product_nav': active_product_nav,
      'product': product,
      'product_types': product_types,
      'absolute_url': request.build_absolute_uri(),
      'fb_og': {
          'image': product.data.get('small_image', None),
          'site_name': b'Phần Mềm Học Tập',
          'description': product.data.get('short_description', None),
          'title': product.data.get('display_name', None),
      },
    })    
    
    return render_to_response('secom/product_detail.html', c)

@login_required
def product_download(request, name=''):
    # get the product
    product = None
    try:
        product = Product.objects.get(active=True, name=name)
    except Product.DoesNotExist:
        raise Http404
    
    if not 'download_link' in product.data:
        raise Http404 
       
    ProductDownload(
        product=product,
        reference=request.GET.get('r', '')
    ).save()
    
    return redirect(product.data['download_link'])


def news(request, path=''):
    config = get_configuration()
    record_per_page = request.GET.get('ppp', config['number_posts_per_page'])
    number_important_posts = request.GET.get('nip', config['number_important_posts'])
    
    page = 1   
    if len(path) > 0:
        # split up to parts
        tags = path.split('/')
        # remove empty parts
        tags = filter(None, tags)        
        # try to get the page parameter
        if tags[-1].startswith('trang-'):
            # remove the last element and get the page value
            page = tags.pop()[6:]
    else:
        tags = []
        
    # get production navigation
    posts_navigation = Block.objects.get(name='posts-navigation').data

    # get the list of posts
    latest_posts = Post.objects.filter(active=True).order_by('created_time', 'id')
    # appending the tags query condition
    if len(tags) > 0:
        latest_posts = latest_posts.filter(tags__name__in=tags)
    latest_posts = latest_posts.distinct('created_time', 'id')
    # create the paginator
    paginator = Paginator(latest_posts, record_per_page)
      
    # get the right page
    try:
        latest_posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        latest_posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        latest_posts = paginator.page(paginator.num_pages)
    
    # handling the paging
    root = '/tin-tuc/loai'  
    if len(tags) > 0:
        root = '/' + root + '/' + '/'.join(tags)        
    next_page = ''
    if latest_posts.has_next():
        next_page = root + '/trang-' + str(latest_posts.next_page_number())
    previous_page = ''
    if latest_posts.has_previous():
        previous_page = root + '/trang-' + str(latest_posts.previous_page_number())
        
    # find import posts
    important_posts = Post.objects \
       .filter(active=True) \
       .exclude(id__in=[p.id for p in latest_posts]) \
       .order_by('priority')[:number_important_posts]       
     
    # find which product-nav to be highlighted
    active_post_nav = None
    matches = filter(lambda n: n['id'] in tags, posts_navigation)
    if len(matches) > 0:
        active_post_nav = matches[0]['id']    
        title = matches[0]['title']
    else:
        title = None
    
    c = get_base_context(request)
    c.update({
        'post_nav': posts_navigation,
        'latest_news': latest_posts,
        'important_posts': important_posts,
        'active_post_nav': active_post_nav,
        'tags': tags,
        'next_page': next_page,
        'previous_page': previous_page,
        'title': title
        })
    return render_to_response('secom/posts.html', c)

def news_detail(request, name=''):
    # get the product
    post = None
    try:
        post = Post.objects.get(active=True, name=name)
    except Post.DoesNotExist:
        raise Http404
    # get post navigation
    posts_navigation = Block.objects.get(name='posts-navigation').data
    # find which post-nav to be highlighted
    active_post_nav = None
    matches = filter(lambda n: n in post.tags.names(), ['khuyen-mai'])
    if len(matches) > 0:
        active_post_nav = matches[0]
        
    # get the related products
    related_products = Product.objects \
      .filter(active=True, tags__name__in=post.tags.names()) \
      .order_by('created_time', 'id') \
      .distinct('created_time', 'id')
    if len(related_products) > 3:
        related_products = random.sample(related_products, 3)
    elif len(related_products) == 0:
        related_products = Product.objects \
            .filter(active=True) \
            .order_by('?')[:3]

    # get the related post
    related_posts = Post.objects \
      .filter(active=True, tags__name__in=post.tags.names()) \
      .order_by('created_time', 'id') \
      .distinct('created_time', 'id')
    if len(related_posts) > 5:
        related_posts = random.sample(related_posts, 5)
    elif len(related_products) == 0:
        related_posts = Post.objects \
            .filter(active=True) \
            .order_by('?')[:5]

    c = get_base_context(request)    
    c.update({
        'post': post,
        'absolute_url': request.build_absolute_uri(),
        'post_nav': posts_navigation,
        'active_post_nav': active_post_nav,
        'related_products': related_products,
        'related_posts': related_posts,
        'fb_og': {
            'image': post.data.get('small_image', None),
            'site_name': b'Phần Mềm Học Tập',
            'description': post.data.get('short_description', None),
            'title': post.data.get('display_name', None),
        },
    })
    
    if 'trailer' in post.data:
        import urlparse
        trailer_url = urlparse.urlparse(post.data['trailer'])
        query = urlparse.parse_qs(trailer_url.query)
        if 'v' in query:
            video_id = query["v"][0]        
            c.update({"trailer": "https://youtube.com/v/%s?version=3&enablejsapi=1" % video_id })     
        
    if 'gallery' in post.data:
        images = post.data['gallery']
        if len(images) > 0:
            c.update({"images": images})

    return render_to_response('secom/post_detail.html', c)

def return_invalid_credential(template, context, email, message='Email hoặc mật khẩu không đúng.'):
    context.update({
        'login_error': message,
        'login_email': email,
    })
    return render_to_response(template, context)

def return_bad_request(request):
    if settings.DEBUG:
        print (traceback.format_exception(*sys.exc_info()))            
    return bad_request(request)


def create_order(request):
    try:
        # create items        
        itemCount = int(request.POST['itemCount'])
        if itemCount <= 0:
            raise ValueError  

        customer = request.user.customer    
        # create order with given customer
        order = Order()
        order.customer = customer
        order.save()
        # update last order time
        customer.last_order_time = now()
        customer.save()
        for i in range(1, itemCount + 1):
            item = OrderItem()
            item.order = order
            item.product = Product.objects.get(id=request.POST['item_id_' + str(i)])
            item.quantity = int(request.POST['item_quantity_' + str(i)])
            item.price = item.product.price()
            item.save()
            # create for combo        
            if item.product.is_combo():
                for sp in item.product.sub_products.all():
                    sitem = OrderItem()
                    sitem.order = order
                    sitem.product = sp.product
                    sitem.quantity = item.quantity                
                    sitem.parent_item = item
                    sitem.save()
        # send confirmation email
        receipt.send_order_created(order)
        return (True, order)
    except ValueError:
        return (False, return_bad_request(request))
    except KeyError:
        return (False, return_bad_request(request))
    except Product.DoesNotExist:            
        return (False, return_bad_request(request))


def checkout_login(request):
    c = get_base_context(request)
    c.update({
        'enable_popup_cart': False,
    })    
    if request.method == "POST":
        # this must be a post back with 'checkout_type' value in POST
        try:
            email = request.POST['email']
            password = request.POST['password']
            # login request
            if request.POST['login_type'] == 'login':
                # try getting the existing user
                try:
                    # get user
                    user = User.objects.get(email=email, is_staff=False)          
                    # authenticate the user
                    user = authenticate(username=user.username, password=password)
                    # wrong credential
                    if not user:
                        return return_invalid_credential('secom/checkout-login.html', c, email)   
                    # account is blocked or staff
                    if not user.is_active or request.user.is_staff:
                        return return_invalid_credential('secom/checkout-login.html', c, email, "Tài khoản khách hàng đã bị khóa. Vui lòng liên hệ hotline để được tư vấn!")                                        
                except User.DoesNotExist:
                    return return_invalid_credential('secom/checkout-login.html', c, email)        
                # everything is ok, we get an existing customer
                customer = user.customer
            # create new request
            elif request.POST['login_type'] == 'register':
                name = request.POST['name']
                try:
                    User.objects.get(email=email)
                    # user already exist
                    c.update({
                        'register_error': 'Email đã tồn tại! <a href="%s">Quên mật khẩu?</a>' % reverse("password_reset"),
                        'register_email': email,
                        'register_name': name,
                    })
                    return render_to_response('secom/checkout-login.html', c)
                except User.DoesNotExist:
                    username = new_code()
                    # creating new user            
                    user = User.objects.create_user(username, email, password)
                    # update name
                    user.first_name = name
                    user.save()
                    # create customer
                    customer = Customer()
                    customer.user = user
                    customer.save()
                    # need to authenticate before login
                    user = authenticate(username=username, password=password)                    
                    # send email
                    customer_email.send_account_created(customer)                    
            else:
                return return_bad_request(request)                    
            # log the user in
            login(request, user)    
        except ValueError:
            return return_bad_request(request)
        except KeyError:
            return return_bad_request(request)
        
        # now create the order 
        (success, result) = create_order(request)
        if success: 
            order = result
            # everything seems fine, now show the detail page with payment
            request.session['clear_local_cart'] = True
            return redirect('order.detail', order.order_code)
        else:
            return result
    else:
        if not request.user.is_authenticated():
            return render_to_response('secom/checkout-login.html', c)
        if not request.user.is_active or request.user.is_staff:
            logout(request)
            c = get_base_context(request)
            c.update({
                'enable_popup_cart': False,
            })
            return render_to_response('secom/checkout-login.html', c)
        # nothing to do with a non-POST request
        return redirect("home")


def checkout(request):
    # we accept POST only
    # however, to ensure customer won't feel weird we will direct to home page
    if request.method == 'GET':
        return redirect("home")

    c = get_base_context(request)
    c.update({
        'enable_popup_cart': False,
    })    
    # in case not yet authenticated, show the login page
    if not request.user.is_authenticated():
        return redirect("checkout.login")
    else:
        # if a user does not link to a customer
        # or it is not valid or a staff account
        # then logout and render as normal 
        if not request.user.is_active or request.user.is_staff:
            logout(request)
            return redirect("checkout.login")

    (success, result) = create_order(request)
    if success: 
        order = result
        # everything seems fine, now show the detail page with payment
        request.session['clear_local_cart'] = True
        return redirect('order.detail', order.order_code)
    else:
        return result



@require_http_methods(['GET', 'POST'])
def customer(request):
    c = get_base_context(request)  
    
    if request.method == "GET" and not request.user.is_authenticated():
        return render_to_response('secom/customer/login.html', c)

    if request.method == "POST":
        try:
            email = request.POST['email']
            password = request.POST['password']
            if request.POST['login_type'] == 'login':
                # log curent user out
                logout(request)
                # try getting the existing user
                try:
                    # get user
                    user = User.objects.get(email=email, is_staff=False)          
                    # authenticate the user
                    user = authenticate(username=user.username, password=password)
                    # wrong credential
                    if not user:
                        return return_invalid_credential('secom/customer/login.html', c, email)   
                    # account is blocked
                    if not user.is_active:
                        return return_invalid_credential('secom/customer/login.html', c, email, "Tài khoản khách hàng đã bị khóa. Vui lòng liên hệ hotline để được tư vấn!")                                        
                except User.DoesNotExist:
                    return return_invalid_credential('secom/customer/login.html', c, email)        
            # create new request
            elif request.POST['login_type'] == 'register':
                # log curent user out
                logout(request)
                name = request.POST['name']
                try:
                    User.objects.get(email=email)
                    # user already exist
                    c.update({
                        'register_error': 'Email đã tồn tại! <a href="%s">Quên mật khẩu?</a>' % reverse("password_reset"),
                        'register_email': email,
                        'register_name': name,
                    })
                    return render_to_response('secom/customer/login.html', c)
                except User.DoesNotExist:
                    username = new_code()
                    # creating new user            
                    user = User.objects.create_user(username, email, password)
                    # update name
                    user.first_name = name
                    user.save()
                    # create customer
                    customer = Customer()
                    customer.user = user
                    customer.save()
                    # send email
                    customer_email.send_account_created(customer)                    
                    # need to authenticate before login
                    user = authenticate(username=username, password=password)
            else:
                return return_bad_request(request)                    
            # log the user in
            login(request, user)
        except ValueError:
            return return_bad_request(request)
        except KeyError:
            return return_bad_request(request)
     
    if 'next' in request.GET:
        return redirect(request.GET['next'])
    return redirect('customer.orders')


def update_user_context(c, request):    
    welcome = ''
    user = request.user
    if user.is_authenticated():
        if not user.is_active or user.is_staff:
            logout(request)
        welcome = u'Chào '
        if len(user.first_name) > 0:
            welcome += user.first_name
        else:
            welcome += user.email.split(u'@')[0]    
        c.update({
            'welcome': welcome,
            'user': user,
        })
    
    if not isinstance(user, AnonymousUser):
        try:
            c.update({
                'customer': user.customer
            })
        except Customer.DoesNotExist:
            pass
    return c

def customer_logout(request):
    logout(request)
    return redirect(request.GET.get('return', '/'))

@login_required
def customer_orders(request):
    if not request.user.is_authenticated():
        return redirect('customer')
    if not request.user.is_active or request.user.is_staff:
        logout(request)
        return redirect('customer')
    
    try:
        customer = request.user.customer
    except Customer.DoesNotExist:
        logout(request)
        return redirect('customer')
    
    orders = Order.objects \
       .filter(customer=customer) \
       .order_by('-created_time')
    
    c = get_base_context(request)
    update_user_context(c, request)    
    
    if len(orders) == 0:
        # get the list of product
        products = Product.objects.filter(active=True).order_by('created_time', 'id')
        if len(products) > 4:
            products = random.sample(products, 4);
        c.update({
            'content_active_item': 'orders',
            'suggestions': products
        })
    else:
        c.update({
            'content_active_item': 'orders',
            'orders': orders
        })
    return render_to_response('secom/customer/orders.html', c)

@login_required
def order_detail(request, code):
    if not request.user.is_authenticated():
        return redirect('customer')
    if not request.user.is_active or request.user.is_staff:
        logout(request)
        return redirect('customer')

    try:
        order = Order.objects.get(order_code=code)
    except Order.DoesNotExist:
        raise Http404

    c = get_base_context(request)
    update_user_context(c, request)
    
    order_root_items = order.orderitem_set.filter(parent_item=None)
    order_items = []
    for i in order_root_items:
        order_items.append(i)
        for si in i.sub_items.all():
            si.pre_pad = '&nbsp;&nbsp;&nbsp;&nbsp;'
            order_items.append(si)    
    c.update({
        'content_active_item': 'orders',
        'enable_popup_cart': False,
        'order': order,
        'order_items': order_items,
    })
        
    if request.method == "GET":
        # determine whether we should clear the local cart
        if 'clear_local_cart' in request.session:
            clear_local_cart = request.session['clear_local_cart']
            del request.session['clear_local_cart']
        else:
            clear_local_cart = False
        
        # list of available banks
        bank_accounts = Block.objects.get(name='bank-accounts').data
        
        c.update({
            'clear_local_cart': clear_local_cart,
            'bank_accounts': bank_accounts
        })        
        return render_to_response('secom/customer/order_detail.html', c)
    else:
        try:            
            if order.status == 0:
                command = request.POST['command']
                if command == 'inform-transfer':
                    # get transfer detail
                    bank_acc_number = request.POST.get('bank-acc-number', '')
                    customer_phone = request.POST.get('customer-phone', '')
                    # user change order status
                    order.status = 2
                    order.save()
                    # create the bank transfer
                    banktransfer = BankTransfer(
                        order=order,
                        customer=order.customer,
                        bank_code=bank_acc_number,
                        tranfer_amount=order.total(),
                        customer_phone=customer_phone
                    ) 
                    banktransfer.save()
                    # send email to customer
                    receipt.send_money_tranferred(order, bank_acc_number, customer_phone)
                elif command == 'cancel':
                    # user change order status
                    order.status = 3
                    order.save()                
                elif command == 'pay':
                    order_total = order.total()
                    enough_keys = True
                    if request.user.customer.total_money >= order_total:
                        # find keys
                        order_items = order.orderitem_set.all()
                        for item in order_items:
                            if item.product.is_combo():
                                # do nothing for the combo product
                                continue
                            else:
                                # find key for the product
                                keys = ProductKey.objects.filter(product=item.product, status=0)
                                if len(keys) >= item.quantity:
                                    for i in range(item.quantity):
                                        item.product_keys.add(keys[i]) 
                                else: 
                                    enough_keys = False
                                    break
                        # not enough key
                        if not enough_keys:
                            # inform customer the order is in pending state
                            order.status = 4
                            order.save()
                            # email saler about the status that need action
                            receipt.send_missing_keys(order)
                        else:
                            for item in order_items:
                                for key in item.product_keys.all():
                                    key.status = 1  # mark sold
                                    key.sold_time = now()
                                    key.save()                            
                                item.save()
                            # mark complete
                            order.status = 1
                            order.save()
                            # update customer money
                            customer = request.user.customer
                            customer.total_money -= order.total()
                            customer.save()                        
                            # email customer about the detail
                            receipt.send_order_completed(order)
                    else:
                        # Bad, people is trying to HACK
                        return return_bad_request(request)
                else:
                    return return_bad_request(request)
            return redirect('order.detail', code=order.order_code)
        except KeyError:
            return return_bad_request(request)


@login_required
def customer_cards(request):
    if not request.user.is_authenticated():
        return redirect('customer')
    if not request.user.is_active or request.user.is_staff:
        logout(request)
        return redirect('customer')

    try:
        customer = request.user.customer
    except Customer.DoesNotExist:
        logout(request)
        return redirect('customer')
    
    cards = CardPayment.objects \
       .filter(customer=customer) \
       .order_by('-created_time')
    
    c = get_base_context(request)
    update_user_context(c, request)    
    c.update({
        'content_active_item': 'cards',
    })
    
    if len(cards) == 0:
        # get the list of product
        products = Product.objects.filter(active=True).order_by('created_time', 'id')
        if len(products) > 4:
            products = random.sample(products, 4);
        c.update({
            'suggestions': products
        })
    else:
        if len(cards) > 10:
            # only show the last 10
            cards = cards[:10]
        c.update({            
            'cards': cards
        })
    return render_to_response('secom/customer/cards.html', c)

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders it's content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@login_required
@require_http_methods(['POST'])
def card(request):
    if not request.user.is_active or request.user.is_staff:
        logout(request)
        return JSONResponse({
                        'status': 'NOK',
                        'error' : 'Tài khoản không được phép thực hiện giao dịch.'
                    })
    # now get the customer
    customer = request.user.customer

    try:
        
        provider = request.POST['provider']
        if provider not in ['VMS', 'VNP', 'VTT']:
            return return_bad_request(request)
        
        number = request.POST['number'].replace("-", "").replace(" ", "")
        serial = request.POST['serial'].replace("-", "").replace(" ", "")
        
        if settings.DEBUG and number == '!@#' and serial == '$%^':
            (success, result) = (True, '10000')    
        else:
            # try to do the charging        
            (success, result) = epay.pay(number, serial, provider, False, False)        
        # create the payment
        payment = CardPayment(
               customer=request.user.customer,
               card_number=number,
               card_serial=serial,
               card_vendor=provider,
            )
        if success:
            if result.startswith('NOK-'):
                # failed, save the payment also
                payment.status = 0
                payment.error_code = result
                payment.save()
                # find the message
                error_code = int(result[4:])
                error_codes = Block.objects.get(name='card-error-codes').data
                founds = [i['message-vn'] for i in error_codes if i['code'] == error_code]
                if len(founds) > 0:
                    error_message = founds[0]
                else:
                    error_message = 'Lỗi trong quá trình nạp thẻ, vui lòng liên hệ hotline để được tư vấn!'
                return JSONResponse({
                        'status': 'NOK',
                        'error' : error_message
                    })
            else:
                try:                
                    # success, save the payment
                    payment.status = 1
                    payment.amount = long(result)
                    payment.save()
                    # and save the customer total_money
                    customer.total_money += payment.amount
                    customer.last_pay_time = now()
                    customer.save()
                    return JSONResponse({
                            'formatted_total_money': formatter.format_price(customer.total_money) + u"đ",
                            'total_money': customer.total_money,
                            'amount': payment.amount,
                            'status': 'OK',
                        })
                except ValueError:
                    # something is wrong with the result => it can not be parsed
                    # failed, save the payment also
                    payment.status = 0
                    payment.error_code = 'BadValue:%s' % result
                    payment.save()
                    return JSONResponse({
                            'status': 'NOK',
                            'error' : 'Lỗi trong quá trình nạp thẻ, vui lòng liên hệ hotline để được tư vấn!'
                        })
        else:
            # failed, save the payment also
            payment.status = 0
            payment.error_code = result
            payment.save()
            return JSONResponse({
                    'status': 'NOK',
                    'error' : 'Lỗi trong quá trình nạp thẻ, vui lòng liên hệ hotline để được tư vấn!'
                })
    except KeyError:
        return return_bad_request(request)
    

class QuestionForm(forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                'required': '',
                'placeholder': 'vd - nguyen.chinh@gmail.com',
                }))
    content = forms.CharField(
        label='Câu hỏi',
        widget=forms.Textarea(
            attrs={
                'required': '',
                'placeholder': 'vd - tôi đã chuyển tiền, giờ tôi phải làm gì?',
            }))
    captcha = ReCaptchaField(
        label='Nhập giá trị trong hình',
        attrs={'theme' : 'white'})

@require_http_methods(['POST'])
def question(request):
    try:
        form = QuestionForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            content = form.cleaned_data['content']
            if len(email) == 0 or len(content) == 0:
                return return_bad_request(request)
                        
            customer_email.send_customer_posted_question(email, content)
            return JSONResponse({
                    'status': 'OK',
                })
        else:
            return JSONResponse({
                    'status': 'NOK',
                    'error' : "thông tin không hợp lệ, vui lòng kiểm tra nếu bạn đã nhập đúng giá trị của mã xác nhận"
                    })
    except KeyError:
        return return_bad_request(request)

@login_required
def import_keys(request):
    if not request.user.is_staff:
        # raise not-found 
        raise Http404
        
    if request.method == "GET":        
        products = Product.objects.filter(active=True).order_by('name')
        c = {
             'products': products
        }
        c.update(csrf(request))
        return render_to_response('secom/admin/import_keys.html', c)
    else:
        try:
            product_id = long(request.POST['product'])
            product = Product.objects.get(id=product_id)
            
#             regex = re.compile("^[A-Z0-9]{5}-[A-Z0-9]{5}-[A-Z0-9]{5}$")
            codes = request.POST['keys'].splitlines()
            
            total_codes = 0
            not_saved_codes = []
            for c in codes:
#                 if regex.match(c):
                c = c.strip()
                if len(c) == 0:
                    continue
                try:
                    key = ProductKey(product=product, key=c)
                    key.save()
                    total_codes += 1
                except Exception, e:
                    not_saved_codes.append({'code': c, 'error': e.message})
#                 else:
#                     not_saved_codes.append({'code': c, 'error': 'Must be in this format XXXXX-YYYYY-ZZZZZ'})

            return JSONResponse({
                    'status': 'OK',
                    'total_codes': total_codes,
                    'not_saved_codes': not_saved_codes
                })
        except KeyError:
            return JSONResponse({
                    'status': 'NOK',
                    'error': 'Missing required parameters'
                })
        except ValueError:
            return JSONResponse({
                    'status': 'NOK',
                    'error': 'Invalid parameters'
                })
        except Product.DoesNotExist:
            return JSONResponse({
                    'status': 'NOK',
                    'error': 'Invalid parameters'
                })
                
    
# ERRORS HANDLERS

def error400(request):
    c = get_base_context(request)
    # get the list of product
    products = Product.objects.filter(active=True).order_by('created_time', 'id')
    if len(products) > 4:
        products = random.sample(products, 4);
    
    c.update({        
      'suggestions': products 
    })    
    return render_to_response('secom/errors/400.html', c)

def error403(request):
    c = get_base_context(request)
    # get the list of product
    products = Product.objects.filter(active=True).order_by('created_time', 'id')
    if len(products) > 4:
        products = random.sample(products, 4);
    
    c.update({        
      'suggestions': products 
    })    
    return render_to_response('secom/errors/403.html', c)

def error404(request):
    c = get_base_context(request)
    # get the list of product
    products = Product.objects.filter(active=True).order_by('created_time', 'id')
    if len(products) > 4:
        products = random.sample(products, 4);
    
    c.update({        
      'suggestions': products 
    })    
    return render_to_response('secom/errors/404.html', c)


def gen_code(request):
    return HttpResponse(new_code())

@csrf_protect
def password_reset(request):
    context = get_base_context(request)
    return auth_views.password_reset(
            request,
            extra_context=context)
    
def password_reset_done(request):
    context = get_base_context(request)
    return auth_views.password_reset_done(
            request,
            extra_context=context)
        
    
@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,):    
    context = get_base_context(request)
    return auth_views.password_reset_confirm(
            request,
            uidb64,
            token,
            extra_context=context)
    
def password_reset_complete(request):
    context = get_base_context(request)
    return auth_views.password_reset_complete(
            request,
            extra_context=context)

@csrf_protect
def password_change(request):
    context = get_base_context(request)
    return auth_views.password_change(
            request,
            extra_context=context)

def password_change_done(request):
    context = get_base_context(request)
    return auth_views.password_change_done(
            request,
            extra_context=context)

def faqs(request, name):
    qna = None
    if name is not None and len(name) > 0:
        try:
            qna = QuestionAndAnswer.objects.get(name=name)
        except QuestionAndAnswer.DoesNotExist:
            pass
            
    c = get_base_context(request)
    c.update({
      'faqs': QuestionAndAnswer.objects.all(),
      'selected': qna,
      'form': QuestionForm()
    })
    return render_to_response('secom/faqs.html', c)


def contact(request):
            
    c = get_base_context(request)
    c.update({
      'form': QuestionForm()
    })
    return render_to_response('secom/contact.html', c)
