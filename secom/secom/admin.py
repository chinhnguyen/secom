# -*- coding: utf-8 -*-
from django.contrib import admin, messages 
from secom.models import *
from secom.mailer import receipt

class ImageAdmin(admin.ModelAdmin):
    list_display = ['thumbnail', 'url', 'uploaded_time']
    readonly_fields = ['uploaded_time']    
    
    def thumbnail(self, obj):
        if obj and obj.image:
            return u'<img src="%s" width="60px"/>' % (obj.url())
        return ''
    thumbnail.allow_tags = True    
    
class SubProductInline(admin.TabularInline):
    model = SubProduct
    fk_name = "main_product"
        
class ProductAdmin(admin.ModelAdmin):    
    list_display = ['name',
                    'keys',
                    'types',
                    'url',
                    'preview',
                    'download_link',
                    'thumbnail',
                    'hot_image',
                    'active',
                    'display_on_home',
                    'display_as_hot_product',
                    'created_time']
    filter = ['tags']
    readonly_fields = ['created_time'] 
    actions = ['duplicate']
    
    change_form_template = 'secom/admin/change_form.html'
        
    def thumbnail(self, obj):
        return u'<img src="%s" width="60px"/>' % (obj.small_image())
    thumbnail.allow_tags = True
    thumbnail.short_description = "hình nhỏ"

    def hot_image(self, obj):
        return u'<img src="%s" width="60px"/>' % (obj.hot_image())
    hot_image.allow_tags = True
    hot_image.short_description = "hình hot"

    def download_link(self, obj):
        return u'<a href="%s">tải về</a>' % (obj.download_link())
    download_link.allow_tags = True
    download_link.short_description = "tải về"

    def url(self, obj):
        _url = obj.url()
        return u'<a href="%s">%s</a>' % (_url, obj.display_name())
    url.allow_tags = True
    url.short_description = "đường dẫn"
    
    def preview(self, obj):
        return u'<a href="/san-pham/xem-truoc/%s" target="preview">Xem</a>' % (obj.name)
    preview.allow_tags = True;
    preview.short_description = "xem trước"
    
    def keys(self, obj):
        if obj.is_combo():
            return None
        return "%d/%d" % (obj.productkey_set.filter(status=0).count(), 
                          obj.productkey_set.count())
    keys.short_description = "mã"
    
    def types(self, obj):
        return ', '.join([str(tag) for tag  in obj.tags.all()])
    types.short_description = "loại"

    # Actions
    def duplicate(self, request, queryset):
        for o in queryset:
            o.id = None
            o.name += "-copy"
            o.save()
    duplicate.short_description = "Tạo sản phẩm tương tự"
    
    inlines = [SubProductInline]


class ProductKeyAdmin(admin.ModelAdmin):
    list_display = ['product',
                    'key',
                    'status',
                    'created_time',
                    'sold_time'] 
    
    readonly_fields = ['created_time',
                       'sold_time']   
    
    list_filter = ['product',
                   'status',
                   'created_time',
                   'sold_time']
    
    search_fields = ['key']
    
    
    
class PostAdmin(admin.ModelAdmin):
    list_display = ['display_name',
                    'name',
                    'thumbnail',
                    'active',
                    'display_on_home',
                    'created_time']
    readonly_fields = ['created_time'] 
    actions = ['duplicate']
        
    change_form_template = 'secom/admin/change_form.html'
    
    def thumbnail(self, obj):
        return u'<img src="%s" width="60px"/>' % (obj.small_image())
    thumbnail.allow_tags = True    

    # Actions
    def duplicate(self, request, queryset):
        for o in queryset:
            o.id = None
            o.name += "-copy"
            o.save()
    duplicate.short_description = "Tạo bài viết tương tự"
    
    
class BlockAdmin(admin.ModelAdmin):
    list_display = ['name',
                    'active',
                    'created_time']
    readonly_fields = ['created_time'] 

class CustomerOrderInline(admin.TabularInline):
    model = Order
    readonly_fields = ['order_code',
                       'created_time',
                       'updated_time',
                       'payment_type',
                       'status']
    can_delete = False
    def has_add_permission(self, request):
        return False
    
    
class CustomerCardPaymentInline(admin.TabularInline):
    model = CardPayment
    readonly_fields = ['status',
                       'card_number',
                       'card_serial',
                       'card_vendor',
                       'amount',
                       'error_code',
                       'created_time']
    can_delete = False
    def has_add_permission(self, request):
        return False

class CustomerAdmin(admin.ModelAdmin):
    list_display = ['email',
                    'name',
                    'total_money',
                    'order_count',
                    'card_payment_count',
                    'last_order_time',
                    'last_pay_time',
                    'date_joined']
    
    readonly_fields = ['last_pay_time',
                       'last_order_time',
                       'user']

    list_filter = ['last_order_time',
                   'last_pay_time',
                   'user__date_joined']
    
    search_fields = ['user__email',
                     'user__first_name']
    
    inlines = [CustomerOrderInline,
               CustomerCardPaymentInline]
    
class OrderItemInline(admin.TabularInline):
    model = OrderItem
    
    fields = ['product',
              'price',
              'quantity',
              'product_keys']
    
    readonly_fields = ['product_keys', 
                       'parent_item']
    
       
class OrderAdmin(admin.ModelAdmin):    
    list_display = ['order_code',
                    'customer_name',
                    'item_count',
                    'total',
                    'status',
                    'payment_type',
                    'created_time']
    
    readonly_fields = ['created_time',
                       'updated_time',
                       'order_code']
    
    list_filter = ['status',
                   'payment_type',
                   'created_time']
    
    search_fields = ['order_code',
                     'customer__user__email',
                     'customer__user__first_name']
    
    inlines = (OrderItemInline,)
    
    actions = ['complete_order', 'resend_email']
    
    def customer_name(self, obj):
        if obj and obj.customer:
            return obj.customer.email()
        return ''
    customer_name.short_description = "khách hàng"
    customer_name.admin_order_field = 'customer__user__first_name'
    
    def complete_order(self, request, queryset):
        for order in queryset:
            if order.status == 1 or order.status == 3:
                continue
            order_total = order.total()
            customer = order.customer
            
            if customer.total_money >= order_total:
                enough_keys = True
                # find keys
                order_items = order.orderitem_set.all()
                for item in order_items:
                    if item.product.is_combo():
                        # do nothing for the combo product
                        continue
                    else:
                        keys = ProductKey.objects.filter(product=item.product, status=0)
                        if len(keys) >= item.quantity:
                            for i in range(item.quantity):
                                item.product_keys.add(keys[i]) 
                        else: 
                            enough_keys = False
                            messages.error(request, 'Không đủ key cho sản phẩm %s ' % item.product)                        
    
                if enough_keys:
                    for item in order_items:
                        for key in item.product_keys.all():
                            key.status = 1  # mark sold
                            key.sold_time = now()
                            key.save()                            
                        item.save()
                    # mark complete
                    order.status = 1
                    order.save()
                    # update customer money
                    customer.total_money -= order.total()
                    customer.save()                        
                    # email customer about the detail
                    receipt.send_order_completed(order)
                    messages.info(request, 'Đã hoàn tất đơn hàng %s' % order)  
            else:
                messages.error(request, 'Không đủ tiền để hoàn tất đơn hàng %s' % order)
                  
    complete_order.short_description = "Hoàn tất đơn hàng"
    
    def resend_email(self, request, queryset):       
        for order in queryset:
            if order.status == 1:
                receipt.send_order_completed(order)
                messages.add_message(request, messages.INFO, 'Đã gửi lại email - đơn hàng %s' % order)    
    resend_email.short_description = "Gửi lại email"

class BankTransferAdmin(admin.ModelAdmin):    
    list_display = ['banktransfer',
                    'customer_name',
                    'item_count',
                    'total',
                    'bank_code',
                    'customer_phone',
                    'tranfer_amount',
                    'status',
                    'created_time']
    
    readonly_fields = ['created_time',
                       'updated_time',
                       'order',
                       'customer']
    
    list_filter = ['status',
                   'created_time']
    
    search_fields = ['banktransfer',
                     'customer__user__email',
                     'customer__user__first_name']
    
    actions = ['complete_order',"cancel_order"]

    def customer_name(self, obj):
        if obj and obj.customer:
            return obj.customer.email()
        return ''
    customer_name.short_description = "khách hàng"
    customer_name.admin_order_field = 'customer__user__first_name'
    
    def complete_order(self, request, queryset):
        for banktransfer in queryset:
            if banktransfer.status != 1:
                banktransfer.customer.total_money += long(banktransfer.tranfer_amount)
                banktransfer.customer.save()
                banktransfer.status = 1
                banktransfer.save()
                messages.info(request, 'Đã hoàn tất chuyển khoản %s' % banktransfer.banktransfer) 
    complete_order.short_description = "Hoàn tất chuyển khoản"

    def cancel_order(self, request, queryset):
        for banktransfer in queryset:
            if banktransfer.status != 2:
                banktransfer.customer.total_money -= long(banktransfer.tranfer_amount)
                banktransfer.customer.save()
                banktransfer.status = 2
                banktransfer.save()
                messages.info(request, 'Đã huỷ chuyển khoản %s' % banktransfer.banktransfer) 
    cancel_order.short_description = "Huỷ chuyển khoản"

class MoneyRewardAdmin(admin.ModelAdmin):    
    list_display = ['customer_name',
                    'money_reward',
                    'reward_program',
                    'status',
                    'created_time']
    
    readonly_fields = ['created_time',
                       'customer_name']
    
    list_filter = ['customer',
                   'reward_program']
    
    search_fields = ['reward_program',
                     'customer__user__email',
                     'customer__user__first_name']

    def customer_name(self, obj):
        if obj and obj.customer:
            return obj.customer.email()
        return ''
    customer_name.short_description = "khách hàng"
    customer_name.admin_order_field = 'customer__user__first_name'
    
    
class CardPaymentAdmin(admin.ModelAdmin):
    list_display = ['card_serial',
                    'card_number',
                    'card_vendor',
                    'customer_name',
                    'amount',
                    'error_code',
                    'created_time']

    readonly_fields = ['card_serial',
                       'card_number',
                       'customer',
                       'card_vendor',
                       'customer_name',
                       'amount',
                       'error_code',
                       'status',
                       'created_time']
    
    list_filter = ['status',
                   'card_vendor',
                   'amount',
                   'created_time']
    
    def customer_name(self, obj):
        if obj and obj.customer:
            return obj.customer.email()
        return ''
    customer_name.short_description = "Khách hàng"
    customer_name.admin_order_field = 'customer__user__email'
    
    
class QuestionAndAnswerAdmin(admin.ModelAdmin):
    change_form_template = 'secom/admin/change_form.html'
    
    
admin.site.register(Image, ImageAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Block, BlockAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(BankTransfer, BankTransferAdmin)
admin.site.register(MoneyReward, MoneyRewardAdmin)
admin.site.register(CardPayment, CardPaymentAdmin)
admin.site.register(ProductKey, ProductKeyAdmin)
admin.site.register(QuestionAndAnswer, QuestionAndAnswerAdmin)
