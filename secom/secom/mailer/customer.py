# -*- coding: utf-8 -*-
'''
Created on Jun 5, 2014

@author: ntr9h
'''
from __future__ import absolute_import
import random
from django.core import mail
from django.template import loader, Context
from secom import settings
from secom.models import Product, Customer
from celery import shared_task

#text_message = u'CÓ GÌ ĐÓ KHÔNG ĐÚNG VỚI CHƯƠNG TRÌNH XEM EMAIL CỦA BẠN!'
from_email = u'Phần mềm học tập <pmht@thuynhanproductions.vn>'
recipient_list = [
    u'Nguyễn Trung Chính <chinh.nguyen@thuynhanproductions.vn>',
    u'Lâm Tùng Thiên Trúc <truc.lam@thuynhanproductions.vn>']
headers = {'Reply-To': u'Phần mềm học tập <pmht@thuynhanproductions.vn>'}

def update_user_context(c, user):    
    welcome = u''
    if len(user.first_name) > 0:
        welcome += user.first_name
    else:
        welcome += user.email.split(u'@')[0]    
    c.update({
        'welcome': welcome,
    })
    return c

def send_account_created(customer):
    return __send_account_created__.delay(customer.id)

# http://theticktalk.wordpress.com/2013/06/29/how-to-email-mrtg-graphs-using-python-smtp-library-gmail/
@shared_task
def __send_account_created__(customer_id):
    if customer_id == None:
        return
    try:
        customer = Customer.objects.get(id=customer_id)
    except Customer.DoesNotExist:
        return    
    if not customer.user:
        return
    
    customer_email = customer.user.email
    customer_name = customer.user.first_name
    
    if len(customer_name) > 0:
        to = customer_name + ' <' + customer_email + '>'
    else:
        to = customer_email
    
    email_template = loader.get_template('secom/mails/account_created.html')
    email_context = Context({ 
                 'customer': customer,
                 'root': "http://%s" % settings.HOST,
                })
    # update some user information 
    update_user_context(email_context, customer.user)
    
    # suggest some products
    products = Product.objects.filter(active=True).order_by('created_time', 'id')
    if len(products) > 3:
        products = random.sample(products, 3);
    email_context.update({
        'suggestions': products
    })
    
    # email parameters
    subject = u'Đăng ký tài khoản phanmemhoctap.vn thành công'
    message = email_template.render(email_context)
    
    connection = mail.get_connection()
    email = mail.EmailMultiAlternatives(
        subject,
        message,
        from_email,
        [to] + recipient_list,
        connection=connection,
        headers=headers)
#     email.attach_alternative(message, "text/html")
    email.content_subtype = "html"
    email.send()
    
def send_customer_posted_question(email, content):
    return __send_customer_posted_question__.delay(email, content)    
    
@shared_task
def __send_customer_posted_question__(email, content):
    if email is None or len(email) == 0 or content is None or len(content) == 0 :
        return
    # email parameters
    subject = u'Câu hỏi từ %s' % email
    
    connection = mail.get_connection()
    email = mail.EmailMultiAlternatives(
        subject,
        content,
        email,
        recipient_list,
        connection=connection,
        headers={'Reply-To': email})
#    email.attach_alternative(content, "text/html")
    email.content_subtype = "html"
    email.send()    
    
# if __name__ == '__main__':
#     send_mail()
# 
# if __name__ == '__main__':
#     pass
