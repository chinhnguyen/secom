# -*- coding: utf-8 -*-
'''
Created on Jun 5, 2014

@author: ntr9h
'''
from __future__ import absolute_import
import itertools, random
from django.core import mail
from django.template import loader, Context
from secom.templatetags import formatter
from secom import settings
from secom.models import Product, Order
from celery import shared_task

from_email = u'Phần mềm học tập <pmht@thuynhanproductions.vn>'
recipient_list = [
    u'Nguyễn Trung Chính <chinh.nguyen@thuynhanproductions.vn>',
    u'Lâm Tùng Thiên Trúc <truc.lam@thuynhanproductions.vn>']
headers = {'Reply-To': u'Phần mềm học tập <pmht@thuynhanproductions.vn>'}

def update_user_context(c, user):    
    welcome = u''
    if len(user.first_name) > 0:
        welcome += user.first_name
    else:
        welcome += user.email.split(u'@')[0]    
    c.update({
        'welcome': welcome,
    })
    return c

# http://theticktalk.wordpress.com/2013/06/29/how-to-email-mrtg-graphs-using-python-smtp-library-gmail/
def send_order_created(order):
    return __send_order_created__.delay(order.id)

@shared_task    
def __send_order_created__(order_id):    
    if order_id == None:
        return
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return    

    customer_email = order.customer.user.email
    customer_name = order.customer.user.first_name
    
    if len(customer_name) > 0:
        to = customer_name + ' <' + customer_email + '>'
    else:
        to = customer_email
    
    email_template = loader.get_template('secom/mails/order_created.html')

    items = order.orderitem_set.all()
    # find similar app
    tags = set(list(itertools.chain(*[item.product.tags.all() for item in items])))
    products = Product.objects \
        .filter(active=True, tags__name__in=tags) \
        .exclude(id__in=[item.product.id for item in items]) \
        .distinct('id') 
    # limit to 4 products
    if len(products) > 4:
        products = random.sample(products, 4);
    
    email_context = Context({ 
                 'order': order,
                 'root': "http://%s" % settings.HOST,
                 'suggestions': products
                })
    # update some user information 
    update_user_context(email_context, order.customer.user)
    
    # email parameters
    subject = u'Đơn hàng %s đã được tạo' % (formatter.format_code(order.order_code))
    message = email_template.render(email_context)
    
    connection = mail.get_connection()
    email = mail.EmailMultiAlternatives(
        subject,
        message,
        from_email,
        [to] + recipient_list,
        connection=connection,
        headers=headers)
    email.content_subtype = "html"
    email.send()


def send_order_completed(order):
    return __send_order_completed__.delay(order.id)

@shared_task    
def __send_order_completed__(order_id):    
    if order_id == None:
        return
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return
        
    customer_email = order.customer.user.email
    customer_name = order.customer.user.first_name
    
    if len(customer_name) > 0:
        to = customer_name + ' <' + customer_email + '>'
    else:
        to = customer_email
    
    email_template = loader.get_template('secom/mails/order_completed.html')

    items = order.orderitem_set.all()
    # find similar app
    tags = set(list(itertools.chain(*[item.product.tags.all() for item in items])))
    products = Product.objects \
        .filter(active=True, tags__name__in=tags) \
        .exclude(id__in=[item.product.id for item in items]) \
        .distinct('id') 
    # limit to 4 products
    if len(products) > 4:
        products = random.sample(products, 4);
    
    email_context = Context({ 
                 'order': order,
                 'root': "http://%s" % settings.HOST,
                 'suggestions': products
                })
    # update some user information 
    update_user_context(email_context, order.customer.user)
    
    # email parameters
    subject = u'Đơn hàng %s đã hoàn tất' % (formatter.format_code(order.order_code))
    message = email_template.render(email_context)
    
    connection = mail.get_connection()
    email = mail.EmailMultiAlternatives(
        subject,
        message,
        from_email,
        [to] + recipient_list,
        connection=connection,
        headers=headers)
    email.content_subtype = "html"
    email.send()    
    
    
def send_missing_keys(order):
    return __send_missing_keys__.delay(order.id)

@shared_task    
def __send_missing_keys__(order_id):    
    if order_id == None:
        return
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return

    email_template = loader.get_template('secom/mails/order_not_enough_keys.html')
    email_context = Context({ 
                 'order': order,
                 'root': "http://%s" % settings.HOST,
                })
    
    # update some user information 
    update_user_context(email_context, order.customer.user)
    # email parameters
    subject = u'Thiếu key (đơn hàng %s)' % (formatter.format_code(order.order_code))
    message = email_template.render(email_context)
    
    connection = mail.get_connection()
    email = mail.EmailMultiAlternatives(
        subject,
        message,
        from_email,
        recipient_list,
        connection=connection,
        headers=headers)
    email.content_subtype = "html"
    email.send()    
    
    
    
def send_money_tranferred(order, bankcode, customerphone):
    return __send_money_tranferred__.delay(order.id, bankcode, customerphone)

@shared_task    
def __send_money_tranferred__(order_id, bankcode, customerphone):    
    if order_id == None:
        return
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return

    email_template = loader.get_template('secom/mails/order_money_transferred.html')
    email_context = Context({ 
                 'order': order,
                 'root': "http://%s" % settings.HOST,
                 'bankcode': bankcode,
                 'customerphone': customerphone,
                })
    
    # update some user information 
    update_user_context(email_context, order.customer.user)
    
    # email parameters
    subject = u'Đã chuyển tiền (đơn hàng %s)' % (formatter.format_code(order.order_code))
    message = email_template.render(email_context)
    connection = mail.get_connection()
    email = mail.EmailMultiAlternatives(
        subject,
        message,
        from_email,
        recipient_list,
        connection=connection,
        headers=headers)
    email.content_subtype = "html"
    email.send()    
    
# if __name__ == '__main__':
#     send_mail()
# 
# if __name__ == '__main__':
#     pass
