'''
Created on May 17, 2014

@author: ntr9h
'''
from django import template

from secom.models import Block

register = template.Library()

@register.inclusion_tag('secom/footer.html')
def footer():
    try:
        # get the content for sliding
        footer = Block.objects.get(name='footer').data    
    except Block.DoesNotExist:
        footer = None

    return {'footer':footer}

@register.inclusion_tag('secom/product_preview.html')
def product_preview(p, t='ver'):
    return {'product': p, 'type': t}

@register.inclusion_tag('secom/post_preview.html')
def post_preview(p, t='ver'):
    return {'post': p, 'type': t}

@register.inclusion_tag('secom/slide_content.html')
def slide_content(contents, prefix=''):
    return {'contents': contents, 'prefix': prefix}

@register.inclusion_tag('secom/small_slide_content.html')
def small_slide_content(contents):
    return {'contents': contents}

@register.inclusion_tag('secom/type_navigation.html')
def type_navigation(navigations, active='', is_IE=False):
    c = {'navigations': navigations,
         'active_item': active,
         'is_IE':is_IE}
    if is_IE and len(navigations) > 1:
        c.update({'subnavigations':navigations[1:]})    
    return c

@register.inclusion_tag('secom/section_title.html')
def section_title(title, is_IE=False, direction='left', color='red', extra_class=None):
    h1_class = direction + '-header'
    if color != 'red':
        h1_class += '-' + color
    if extra_class:
        h1_class += ' ' + extra_class
    
    c = {'title': title,
         'is_IE': is_IE,
         'color': color,
         'direction': direction,
         'h1_class':h1_class,
         'extra_class':extra_class}
    return c

