'''
Created on May 16, 2014

@author: ntr9h
'''
from django.template import Library
from django.core.serializers import serialize
from django.db.models.query import QuerySet
import json

register = Library()

@register.filter()
def format_price(value, separator='.', precision=0, delimeter_count=3, decimal_separator=','):
    if value is None:
        return ""
    f = ''
    if isinstance(value, float):
        negative = value < 0
        s = '%s.%df' % ('%', precision) % (abs(value))
        p = s.find(decimal_separator)
        if p > -1:
            f = s[p:]
            s = s[:p]
    elif isinstance(value, int) or isinstance(value, long):
        negative = value < 0
        s = str(abs(value))
    else:
        negative = False
        s = value
        p = s.find(decimal_separator)
        if p > -1:
            f = s[p:p + precision + 1]
            if f == decimal_separator:
                f = ''
            s = s[:p]

    groups = []
    while s:
        groups.insert(0, s[-delimeter_count:])
        s = s[:-delimeter_count]

    return '%s%s%s' % ('-' if negative else '', separator.join(groups), f)

@register.filter()
def format_date(value):
    if value:
        return value.strftime("%d/%m/%Y")
    return ''
    
@register.filter()
def format_code(value):
    if value:
        value = unicode(value)
        return '-'.join([value[0 + i:3 + i] for i in range(0, len(value), 3)])
    return ''

@register.filter
def item_at(l, index):
    return l[index]

@register.filter
def jsonify(o):
    if isinstance(o, QuerySet):
        return serialize('json', o)
    return json.dumps(o, ensure_ascii=False)
