'''
Created on Jul 30, 2014

@author: admin
'''
from django.core.management.base import BaseCommand, CommandError
from secom.models import Product

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for p in Product.objects.all():
            cnt = 0
            if 'gallery' in p.data:
                gallery = p.data['gallery']
                for i in gallery:    
                    if 'src' not in i and 'source' in i:                
                        i['src'] = i.pop('source')
                        cnt += 1
            if cnt > 0:
                p.save()
            self.stdout.write('Patched %d images in %s' % (cnt, p.display_name()))