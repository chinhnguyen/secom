from __future__ import absolute_import
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app

import pytz, uuid
from datetime import datetime
from secom.settings import TIME_ZONE
from secom import fpe

default_timezone = pytz.timezone(TIME_ZONE)

def now():
    return datetime.now(default_timezone)

def new_code():
    _unique_id = uuid.uuid4().int;
    return fpe.instance.encrypt(_unique_id)
