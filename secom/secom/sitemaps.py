'''
Created on Jul 28, 2014

@author: admin
'''
from django.contrib.admin.models import LogEntry
from django.core.urlresolvers import reverse
from django.contrib.sitemaps import Sitemap
from secom.models import Product, Post, Block

from itertools import chain

class NavigationSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0
    
    __lastmod__ = None
    __name__ = None

    def __init__(self, name):
        self.__name__ = name

    def items(self):
        try: 
            config = Block.objects.get(name=self.__name__)
            try:
                self.__lastmod__ = LogEntry.objects \
                    .filter(object_id=config.id) \
                    .values('action_time') \
                    .order_by('-action_time')[0]['action_time']
            except:
                self.__lastmod__ = config.created_time
            # return the tags
            return config.data
        except Block.DoesNotExist:
            return []

    def lastmod(self, nav):
        return self.__lastmod__
        
    def location(self, nav):
        try:
            return nav['url']
        except KeyError:
            return ''


class ProductSitemap(Sitemap):
    changefreq = "daily"
    priority = 1.0

    def items(self):
        return Product.objects.filter(active=True)

    def lastmod(self, product):
        try:
            return LogEntry.objects \
                .filter(object_id=product.id) \
                .values('action_time') \
                .order_by('-action_time')[0]['action_time']
        except:
            return product.created_time
        
    def location(self, product):
        return reverse('product.detail', args=(product.name,))


class ProductListSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.8
    
    __lastmod__ = None

    def items(self):
        try: 
            config = Block.objects.get(name='product-filters')
            try:
                self.__lastmod__ = LogEntry.objects \
                    .filter(object_id=config.id) \
                    .values('action_time') \
                    .order_by('-action_time')[0]['action_time']
            except:
                self.__lastmod__ = config.created_time
            # return the tags
            return [t['value'] for t in chain(*[tg['values'] for tg in config.data])]
        except Block.DoesNotExist:
            return []

    def lastmod(self, type):
        return self.__lastmod__
        
    def location(self, type):
        return reverse('products.list.path', args=(type,))


class PostSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Post.objects.filter(active=True)

    def lastmod(self, post):
        try:
            return LogEntry.objects \
                .filter(object_id=post.id) \
                .values('action_time') \
                .order_by('-action_time')[0]['action_time']
        except:
            return post.created_time
        
    def location(self, product):
        return reverse('news.detail', args=(product.name,))
    
    
    
