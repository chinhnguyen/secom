# -*- coding: utf-8 -*-
'''
Created on May 9, 2014

@author: ntr9h
'''

from django.db import models
from django.db.models.fields import CharField
from django.db.models import Sum
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from jsonfield import JSONField
from taggit.managers import TaggableManager
from secom import now, new_code

jsonfield_kwargs = dict({
    'cls': DjangoJSONEncoder,
    'separators': (',', ':'),
    'ensure_ascii': False})

class Image(models.Model):
    class Meta:
        verbose_name = "hình ảnh"
        verbose_name_plural = "hình ảnh"

    image = models.ImageField(upload_to='images', null=True, blank=True)
    uploaded_time = models.DateTimeField(default=now())
    
    def __unicode__(self):
        return self.url()
    
    def url(self):
        return self.image.url
    

def field(obj, name):
    if obj.data and name in obj.data:   
        return obj.data[name]
    return ''

product_default_json = \
'{ \
  "short_description":"", \
  "display_name":"", \
  "price":0, \
  "small_image":"", \
  "upgrade_guide":"", \
  "gallery": [ { "source": "", "title":"" } ], \
  "trailer": "" \
}'  

class Product(models.Model):
    class Meta:
        verbose_name = "sản phẩm"
        verbose_name_plural = "sản phẩm"
    
    name = models.CharField(
        unique=True,
        max_length=100,
        help_text='vd: hoc-cung-bi-vat-ly-6',
        verbose_name='Mã sản phẩm')
    
    data = JSONField(
        null=True,
        blank=True,
        help_text='Thông tin cơ bản (JSON)',
        verbose_name='Cơ bản',
        default=product_default_json,
        dump_kwargs=jsonfield_kwargs)
    
    description = models.TextField(
        null=True,
        blank=True,
        help_text='Thông tin chi tiết (HTML)',
        verbose_name='Chi tiết')
    
    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Thời gian tạo')
    
    display_on_home = models.BooleanField(
        default=False,
        verbose_name='Trang chủ',
        help_text='Cho phép sản phẩm này hiển thị ngẫu nhiên trên trang chủ')
    
    display_as_hot_product = models.BooleanField(
        default=False,
        verbose_name='Hot',
        help_text='Cho phép sản phẩm này hiển thị như sản phẩm chủ lực trên trang sản phẩm')
    
    active = models.BooleanField(
        default=False,
        verbose_name='Hiệu lực',
        help_text='')
    
    tags = TaggableManager()
    tags.verbose_name = 'Loại'
    tags.help_text = 'vd: hoc-cung-bi, vat-ly, lop-9, ipad, windows'
    
    def __unicode__(self):
        return self.display_name()
    
    def display_name(self):
        return field(self, 'display_name')
    display_name.verbose_name = "Tên sản phẩm"
    
    def price(self):        
        return int(field(self, 'price'))

    def small_image(self):
        return field(self, 'small_image')
       
    def hot_image(self):
        return field(self, 'hot_image')
    
    def download_link(self):
        return field(self, 'download_link')
    
    def image(self):
        if len(self.hot_image()) > 0:
            return self.hot_image()
        else:
            return self.small_image()
    
    def url(self):
        return '/san-pham/' + self.name
    
    def is_combo(self):
        return self.sub_products.count() > 0
    
    def sub_total(self):
        total = 0
        for sp in self.sub_products.all():
            total += sp.product.price()
        return total

    def upgrade_guide(self):
        return field(self, 'upgrade_guide')

class SubProduct(models.Model):
    class Meta:
        verbose_name = "sản phẩm phụ"
        verbose_name_plural = "sản phẩm phụ"

    main_product = models.ForeignKey(Product, related_name='sub_products')

    product = models.ForeignKey(Product,
        related_name='product',
        verbose_name="sản phẩm")
    
    def __unicode__(self):
        if self.product is not None:
            return self.product.display_name()
        return ""
    

class ProductKey(models.Model):
    class Meta:
        verbose_name = "mã kích hoạt"
        verbose_name_plural = "mã kích hoạt"

    def __unicode__(self):
        return self.key
    
    product = models.ForeignKey(Product)
    
    status = models.IntegerField(
        default=0,
        verbose_name='Trạng thái',
        choices=(
            (0, 'Chưa bán'),
            (1, 'Đã bán'),
        ))

    key = models.CharField(
        unique=True,
        max_length=100,
        verbose_name='mã kích hoạt')
    
    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Thời gian tạo')
    
    sold_time = models.DateTimeField(
        null=True,
        verbose_name='Thời gian bán')    
    
class Customer(models.Model):
    class Meta:
        verbose_name = "khách hàng"
        verbose_name_plural = "khách hàng"
        
    def __unicode__(self):
        return self.user.email
    
    # https://docs.djangoproject.com/en/dev/topics/auth/customizing/#extending-the-existing-user-model
    user = models.OneToOneField(User)
    
    def email(self):
        return self.user.email
    email.short_description = "email"
    
    def name(self):
        return self.user.first_name
    name.short_description = "tên"
    
    def date_joined(self):
        return self.user.date_joined
    date_joined.short_description = "ngày tạo"
    date_joined.admin_order_field = 'user__date_joined'
    
    # amount of money left for this account
    total_money = models.BigIntegerField(
        default=0,
        verbose_name='tài khoản')
    
    last_order_time = models.DateTimeField(
        null=True,
        verbose_name='lần cuối đặt hàng')

    last_pay_time = models.DateTimeField(
        null=True,
        verbose_name='lần cuối thanh toán')
            
    def order_count(self):
        return self.order_set.count()
    order_count.short_description = "đơn hàng" 
    
    def card_payment_count(self):
        return self.cardpayment_set.count()
    card_payment_count.short_description = "nạp thẻ" 

class Order(models.Model):
    class Meta:
        verbose_name = "đơn hàng"
        verbose_name_plural = "đơn hàng"
        
    def __unicode__(self):
        if self.customer:
            return self.customer.user.username + "-" + unicode(self.id)
        return ''
    
    order_code = models.BigIntegerField(
        default=new_code,
        verbose_name="Mã đơn hàng")
    customer = models.ForeignKey(Customer, null=True)
    # status
    status = models.IntegerField(
        default=0,
        verbose_name='Trạng thái',
        choices=(
            (0, 'Mới tạo'),
            (1, 'Hoàn tất'),
            (2, 'Đã chuyển tiền'),
            (3, 'Đã hủy'),
            (4, 'Đang được xử lý'),
        ))
     
    payment_type = models.IntegerField(
        max_length=10,
        default=0,
        verbose_name='Loại thanh toán',
        choices=(
            (0, 'Thẻ cào'),
            (1, 'Chuyển khoản'),
        ))
        
    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Thời gian tạo')
    
    updated_time = models.DateTimeField(
        null=True,
        verbose_name='Cập nhật lúc')    
    
    def item_count(self):
        return self.orderitem_set.all().aggregate(Sum('quantity'))['quantity__sum']
    item_count.short_description = "Sản phẩm"
    
    def total(self):
        return self.orderitem_set.aggregate(total=Sum('price', field='quantity*price'))['total']
    total.short_description = "Thành tiền"
    
    def items_html(self):
        return "<br/>".join([i.as_html() for i in self.orderitem_set.all() if i.parent_item == None])

class BankTransfer(models.Model):        
    class Meta:
        verbose_name = "chuyển khoản"
        verbose_name_plural = "chuyển khoản"
    # by who 
    customer = models.ForeignKey(Customer)
    # for what    
    order = models.ForeignKey(Order,
        verbose_name='Mã đơn hàng')

    banktransfer = models.BigIntegerField(
        default=new_code,
        verbose_name='Mã chuyển tiền')

    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Thời gian tạo')

    updated_time = models.DateTimeField(
        null=True,
        verbose_name='Cập nhật lúc') 

    # status
    status = models.IntegerField(
        default=0,
        verbose_name='Trạng thái',
        choices=(
            (0, 'Mới tạo'),
            (1, 'Đã nhận tiền'),
            (2, 'Đã hủy'),
            (3, 'Đang được xử lý'),
    ))

    bank_code = models.CharField(
       max_length=100,
       null=True,
       blank=True,
       verbose_name="Số tài khoản")

    customer_phone = models.CharField(
       max_length=100,
       null=True,
       blank=True,
       verbose_name="Số điện thoại")

    tranfer_amount = models.CharField(
       default=0,
       max_length=100,
       null=True,
       blank=True,
       verbose_name="Số tiền chuyển")

    def item_count(self):
        return self.order.orderitem_set.all().aggregate(Sum('quantity'))['quantity__sum']
    item_count.short_description = "Sản phẩm"
    
    def total(self):
        return self.order.orderitem_set.aggregate(total=Sum('price', field='quantity*price'))['total']
    total.short_description = "Thành tiền"

    def items_html(self):
        return "<br/>".join([i.as_html() for i in self.order.orderitem_set.all() if i.parent_item == None])

class MoneyReward(models.Model):
    class Meta:
        verbose_name = "tiền thưởng"
        verbose_name_plural = "tiền thưởng"

    # by who 
    customer = models.ForeignKey(Customer)

    money_reward = models.CharField(
       default=0,
       max_length=100,
       null=True,
       blank=True,
       verbose_name="Tiền thưởng")

    reward_program = models.CharField(
       default=0,
       max_length=100,
       null=True,
       blank=True,
       verbose_name="Chương trình")

    status = models.IntegerField(
        default=0,
        verbose_name='Trạng thái',
        choices=(
            (0, 'Mới tạo'),
            (1, 'Đã kích hoạt'),
            (2, 'Đã hủy'),
            (3, 'Đang được xử lý'),
    ))

    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Thời gian tạo')
    
class OrderItem(models.Model):
    class Meta:
        verbose_name = "sản phẩm"
        verbose_name_plural = "sản phẩm"
   
    order = models.ForeignKey(Order)
     
    product = models.ForeignKey(
        Product,
        verbose_name="sản phẩm")
    
    price = models.BigIntegerField(
        default=0,
        verbose_name="đơn giá")
    
    quantity = models.IntegerField(
        default=0,
        verbose_name="số lượng")
        
    product_keys = models.ManyToManyField(ProductKey)
    parent_item = models.ForeignKey("self", null=True, related_name='sub_items')
    
    def is_sub(self):
        return self.parent_item is not None
    
    def __unicode__(self):
        return str(self.quantity) + ' x ' + self.product.display_name() + ' x ' + str(self.price)
    
    def as_html(self): 
        html = '<b>' + str(self.quantity) + ' x </b>' + self.product.display_name()
        
        return html
    
    def keys_as_html(self):
        return '<br/>'.join([k.key for k in self.product_keys.all()])

class CardPayment(models.Model):
    class Meta:
        verbose_name = "thẻ cào"
        verbose_name_plural = "thẻ cào"

    def __unicode__(self):
        return self.card_vendor + '-' + self.card_serial 

    # by who 
    customer = models.ForeignKey(Customer)
    # 0: failed, 1: sucess
    status = models.SmallIntegerField(
        max_length=11,
        null=True,
        blank=True,
        verbose_name="kết quả",
        choices=(
            (0, 'Thất bại'),
            (1, 'Thành công'),
        ))
    
    # card info 
    card_number = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name="mã thẻ")
    
    card_serial = models.CharField(
       max_length=100,
       null=True,
       blank=True,
       verbose_name="seri thẻ")
    
    card_vendor = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        verbose_name="nhà cung cấp",
        choices=(
            ('VMS', 'Mobifone'),
            ('VNP', 'Vinaphone'),
            ('VTT', 'Viettel'),
        ))
    
    # amount of money in the card
    amount = models.BigIntegerField(
        max_length=11,
        null=True,
        blank=True,
        verbose_name="trị giá thẻ")
    
    error_code = models.CharField(
        max_length=100,
        null=True,
        blank=True,
        verbose_name="Lỗi")

    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Thời gian nạp')
    
post_default_json = \
'{ \
  "short_description":"", \
  "display_name":"", \
  "small_image":"" \
}'  
     
class Post(models.Model):
    class Meta:
        verbose_name = "bài viết"
        verbose_name_plural = "bài viết"
    
    name = models.CharField(
        unique=True,
        max_length=100,
        help_text='vd: hoc-cung-bi-vat-ly-6',
        verbose_name='Mã')
    
    data = JSONField(
        null=True,
        blank=True,
        help_text='Thông tin cơ bản (JSON)',
        verbose_name='Cơ bản',
        default=post_default_json,
        dump_kwargs=jsonfield_kwargs)
    
    description = models.TextField(
        null=True,
        blank=True,
        help_text='Mô tả ngắn gọn (HTML)',
        verbose_name='Mô tả')
    
    content = models.TextField(
        null=True,
        blank=True,
        help_text='Bài chi tiết (HTML)',
        verbose_name='Nội dung')
    
    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Ngày tạo')
    
    display_on_home = models.BooleanField(
        default=False,
        verbose_name='Hiển thị trên trang chủ',
        help_text='Cho phép bài viết này hiển thị ngẫu nhiên trên trang chủ')

    priority = models.IntegerField(
        default=0,
        verbose_name='Độ ưu tiên',
        help_text='Dùng để hiển thị trong khối tin quan trọng')

    active = models.BooleanField(
        default=False,
        verbose_name='Hiệu lực')
    
    tags = TaggableManager()
    tags.verbose_name = 'Thể loại'
    tags.help_text = 'vd: huong-dan, khuyen-mai'    

    def __unicode__(self):
        return self.name    
    
    def display_name(self):
        return field(self, 'display_name')
    display_name.verbose_name = "Tiêu đề"

    def small_image(self):
        return field(self, 'small_image')

    def detail_url(self):
        return "/tin-tuc/" + self.name
    
   
class Block(models.Model):
    class Meta:
        verbose_name = "cấu hình"
        verbose_name_plural = "cấu hình"

    name = CharField(
        max_length=30,
        verbose_name='Tên',
        help_text='vd:cms-top-navigation-bar')
        
    data = JSONField(
        null=True,
        blank=True,
        verbose_name='Nội dung (JSON)',
        help_text='Nội dung của thiết lập ở dạng JSON',
        dump_kwargs=jsonfield_kwargs)
    
    created_time = models.DateTimeField(
        default=now(),
        verbose_name='Ngày tạo')
    
    active = models.BooleanField(
        default=False,
        verbose_name='Hiệu lực')
    
    def __unicode__(self):
        return self.name    


class ProductDownload(models.Model):
    product = models.ForeignKey(Product)

    reference = CharField(
        max_length=30,
        verbose_name='tham chiếu')

    download_time = models.DateTimeField(
        default=now(),
        verbose_name='ngày tải')
    
class QuestionAndAnswer(models.Model):
    class Meta:
        verbose_name = "hỏi đáp"
        verbose_name_plural = "hỏi đáp"

    def __unicode__(self):
        return self.name    

    name = models.CharField(
        unique=True,
        max_length=100,
        help_text='vd: tien-thua-cua-tui-dau',
        verbose_name='Hỏi')
    
    data = JSONField(
        null=True,
        blank=True,
        help_text='Thông tin cơ bản (JSON)',
        verbose_name='Cơ bản',
        dump_kwargs=jsonfield_kwargs)
    
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name='Trả lời') 
    
    
    
    
