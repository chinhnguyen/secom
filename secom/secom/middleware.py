'''
Created on Jun 24, 2014

@author: ntr9h
'''
class IsIEMiddleware():
    def process_request(self, request):
        if request.META.has_key('HTTP_USER_AGENT'):
            user_agent = request.META['HTTP_USER_AGENT'].lower()
            request.is_IE = ('trident' in user_agent) or ('msie' in user_agent)
            