from django.conf import settings
from django.conf.urls import patterns, include, url, handler404, handler400, handler403
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

from secom import views
from secom import sitemaps

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),

    url(r'^tin-tuc/$', views.news, name='news'),
    url(r'^tin-tuc/loai$', views.news, name='news.list'),
    url(r'^tin-tuc/loai/$', views.news, name='news.list'),
    url(r'^tin-tuc/loai/(?P<path>.+)', views.news, name='news.list.type'),
    url(r'^tin-tuc/(?P<name>.+)', views.news_detail, name='news.detail'),

    url(r'^san-pham/$', views.products, name='products'),
    url(r'^san-pham/danh-sach$', views.products, name='products.list'),
    url(r'^san-pham/danh-sach/$', views.products, name='products.list'),
    url(r'^san-pham/danh-sach/(?P<path>.+)$', views.products, name='products.list.path'),
    url(r'^san-pham/xem-truoc/(?P<name>.+)', views.product_preview, name='product.preview'),
    url(r'^san-pham/tai-ve/(?P<name>.+)', views.product_download, name='product.download'),
    url(r'^san-pham/(?P<name>.+)', views.product_detail, name='product.detail'),
        
    url(r'^thanh-toan/$', views.checkout, name='checkout'),
    url(r'^thanh-toan/dang-nhap$', views.checkout_login, name='checkout.login'),
    url(r'^gen-code/$', views.gen_code, name='gen_code'),

    url(r'^khach-hang/$',
        views.customer,
        name='customer'),
    url(r'^khach-hang/quen-mat-khau/$',
        views.password_reset,
        name='password_reset'),
    url(r'^khach-hang/da-dang-ky-thay-doi-mat-khau/$',
        views.password_reset_done,
        name='password_reset_done'),
    url(r'^khach-hang/doi-mat-khau/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm,
        name='password_reset_confirm'),
    url(r'^khach-hang/thay-doi-mat-khau-thanh-cong/$',
        views.password_reset_complete,
        name='password_reset_complete'),
    url(r'khach-hang/doi-mat-khau/$',
        views.password_change,
        name='password_change'),
    url(r'^khach-hang/da-thay-doi-doi-mat-khau/$',
        views.password_change_done,
        name='password_change_done'),
                       
    
    url(r'^thoat/$', views.customer_logout, name='customer.logout'),
    url(r'^don-hang/$', views.customer_orders, name='customer.orders'),
    url(r'^don-hang/(?P<code>\d+)', views.order_detail, name='order.detail'),
    url(r'^the-da-nap/$', views.customer_cards, name='customer.cards'),

    url(r'^hoi-dap/(?P<name>.*)$', views.faqs, name='faqs'),
    
    url(r'^lien-he/$', views.contact, name='contact'),

    # importing
    url(r'^import-keys/$', views.import_keys, name='import.keys'),

    # AJAX calls
    url(r'^card/$', views.card, name='card'),
    url(r'^question/$', views.question, name='question'),
    
    # other apps    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^error/404', views.error404),
    url(r'^ckeditor/', include('ckeditor.urls')),
    
#    https://docs.djangoproject.com/en/dev/ref/contrib/sitemaps/
    url(r'^sitemap\.xml$',
        'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {
            'top-navigation': sitemaps.NavigationSitemap('top-navigation'),
            'products-navigation': sitemaps.NavigationSitemap('products-navigation'),
            'product': sitemaps.ProductSitemap,
            'product-list': sitemaps.ProductListSitemap,            
            'post': sitemaps.PostSitemap,
        }},
        name='django.contrib.sitemaps.views.sitemap'),

#    https://pypi.python.org/pypi/django-robots              
    url(r'^robots\.txt$', include('robots.urls')),
)

handler404 = views.error404
handler403 = views.error403
handler400 = views.error400
