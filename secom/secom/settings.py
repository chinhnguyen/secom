"""
Django settings for secom project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

from __future__ import absolute_import


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=+%&pfytm+-zcn!q73h89!ff53!=l(u!2i=+w9$bj+_%6vexiw'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'secom',
    'taggit',
    'django_wysiwyg',
    'ckeditor',
    'kombu.transport.django',
    'captcha',
    'robots',
    'compressor',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'secom.middleware.IsIEMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
)

ROOT_URLCONF = 'secom.urls'

WSGI_APPLICATION = 'secom.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'vi-vn'

TIME_ZONE = 'Asia/Ho_Chi_Minh'

USE_I18N = True

USE_TZ = True

USE_THOUSAND_SEPARATOR = True

LOCALE_NAME = 'vi'

# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-USE_L10N
USE_L10N = True
# DATETIME_FORMAT = '%d/%m/%Y %H:%M:%S'

FORMAT_MODULE_PATH = 'secom.formats'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

# Templates
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

# https://docs.djangoproject.com/en/1.6/ref/settings/#std:setting-MEDIA_ROOT
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

MEDIA_URL = "/media/"

# https://github.com/pydanny/django-wysiwyg#installation
DJANGO_WYSIWYG_FLAVOR = "ckeditor"

# https://github.com/shaunsephton/django-ckeditor
CKEDITOR_UPLOAD_PATH = os.path.join(BASE_DIR, "media")
CKEDITOR_IMAGE_BACKEND = 'pillow'


# https://docs.djangoproject.com/en/dev/topics/email/
EMAIL_HOST = "smtp.gmail.com" 
EMAIL_PORT = 587 
EMAIL_HOST_USER = "mailer@phanmemhoctap.vn" 
EMAIL_HOST_PASSWORD = "1Bisnothard"
EMAIL_USE_TLS = True

HOST = "localhost:8000"

# https://docs.djangoproject.com/en/1.2/topics/auth/
LOGIN_URL = 'customer'

# Celery 
# http://docs.celeryproject.org/en/latest/getting-started/brokers/django.html#broker-django
BROKER_URL = 'django://'
# CELERY_RESULT_BACKEND = 'django://'
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'
# CELERY_ACCEPT_CONTENT = ['json']
# CELERY_TIMEZONE = TIME_ZONE
# CELERY_ENABLE_UTC = True

# https://github.com/praekelt/django-recaptcha

RECAPTCHA_PUBLIC_KEY = "6LfQkfUSAAAAACkooqHF-rZNU8FwhCNXVx0rxZ8z"
RECAPTCHA_PRIVATE_KEY = "6LfQkfUSAAAAABHaCfOCoBLdonLktn8JeElzPxNq"
RECAPTCHA_USE_SSL = True
CAPTCHA_AJAX = True

# Database
import dj_database_url
DATABASES = {
    'default': dj_database_url.config(default='postgres://postgres:123@localhost:5432/edu-soft.vn')
}

# https://docs.djangoproject.com/en/dev/ref/contrib/sites/#module-django.contrib.sites
SITE_ID = 1

# https://pypi.python.org/pypi/django-htmlmin
HTML_MINIFY = True

# http://django-compressor.readthedocs.org/en/latest/settings/
COMPRESS_ROOT = os.path.join(BASE_DIR, "static")
COMPRESS_ENABLED = True
# COMPRESS_OFFLINE = True
COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter'
]
COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter'
]
COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile} {outfile}'),
    ('text/x-sass', 'sass --compass {infile} {outfile}'),
)

try:
    # override with real-env config
    from .config import *
except ImportError:
    pass