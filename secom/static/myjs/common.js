function add_script(script) {
	var t = document.createElement("script");
	t.type = "text/javascript";
	t.async = true;
	t.src = script;
	var n = document.getElementsByTagName("script")[0];
	n.parentNode.insertBefore(t, n);
};

function setupCart(static_path, columns) {
	// Setup currency
	simpleCart.currency({
		code : "VND",
		name : "Đồng",
		symbol : "đ",
		delimiter : ".",
		decimal : ",",
		after : true,
		accuracy : 0
	});

	simpleCart.extendCheckout({
		SendForm : function(opts) {
			// url required
			if (!opts.url) {
				return simpleCart.error('URL required for SendForm Checkout');
			}

			// build basic form options
			var data = {
				itemCount : simpleCart.find({}).length
			}, action = opts.url, method = opts.method === "GET" ? "GET" : "POST";

			// add items to data
			simpleCart.each(function(item, x) {
				var counter = x + 1, options_list = [], send;
				data['item_id_' + counter] = item.get('id');
				data['item_quantity_' + counter] = item.quantity();
			});

			// check for return and success URLs in the options
			if (opts.success) {
				data['return'] = opts.success;
			}
			if (opts.cancel) {
				data.cancel_return = opts.cancel;
			}

			if (opts.extra_data) {
				data = simpleCart.extend(data, opts.extra_data);
			}

			// return the data for the checkout form
			return {
				action : action,
				method : method,
				data : data
			};
		}
	});

	if ( typeof columns == 'undefined')
		columns = [{
			view : function(item, column) {
				return '<img class="simpleCart_remove" src="' + static_path + 'secom/img/minus.png" title="Bỏ sản phẩm ra khỏi giỏ hàng"/>';
			},
			label : false
		}, {
			attr : "thumb",
			label : false,
			view : function(item, column) {
				return '<a href="' + item.get('link') + '" target="product"><img src="' + item.get('thumb') + '"/></a>';
			}
		}, {
			attr : 'custom-title',
			label : "Sản phẩm",
			view : function(item, column) {
				return '<a href="' + item.get('link') + '" target="product">' + item.get('name') + '</a><p>' + item.get('description') + '</p>';
			}
		}, {
			attr : "price",
			label : "Giá",
			view : 'currency'
		}, {
			attr : "quantity",
			label : "Số lượng",
			view : function(item, column) {
				return '<input type="number" min="0" max="99" value="' + item.quantity() + '" class="simpleCart_input"/>';
			}
		}, {
			attr : "total",
			label : "Tổng cộng",
			view : 'currency'
		}];

	// init cart
	simpleCart({
		checkout : {
			type : "SendForm",
			url : "/thanh-toan/",
			extra_data : {
				csrfmiddlewaretoken : $.cookie('csrftoken')
			}
		},
		beforeAdd : function(item) {
			$.blockUI({
				message : null,
				overlayCSS : {
					opacity : 0
				}
			});
		},
		afterAdd : function(item) {
			$.unblockUI();
			$.magnificPopup.open({
				removalDelay : 300,
				mainClass : 'mfp-zoom-in',
				closeMarkup : '<img class="mfp-close" src="' + static_path + 'secom/img/close.png"/>',
				items : [{
					src : "#cart-popup",
					type : "inline"
				}],
				callbacks : {
					open : function() {
						$("#cart-popup #message").text("Đã thêm \"" + item.get('name') + "\" vào giỏ hàng.").show();
					},
					close : function() {
						$("#cart-popup #message").text("").hide();
					},
				}
			});
		},
		update : function() {
			if (simpleCart.quantity() > 0) {
				$("#cart-popup #empty-cart").hide();
				$("#cart-popup #cart-content").show();
			} else {
				$("#cart-popup #empty-cart").show();
				$("#cart-popup #cart-content").hide();
			}
		},
		cartStyle : "table",
		cartColumns : columns,
	});
}

function submitWithTags(all) {
	var path = "/san-pham/danh-sach/";
	if (!all) {
		$('input[name="tags"]:checked').each(function() {
			path += this.value + '/';
		});
	}
	window.location.href = path;
}

// to defear the loading of stylesheets
// just add it right before the </body> tag
// and before any js inclusion (for performance)
function loadStyleSheet(src) {
	if (document.createStyleSheet)
		document.createStyleSheet(src);
	else {
		var stylesheet = document.createElement('link');
		stylesheet.href = src;
		stylesheet.rel = 'stylesheet';
		stylesheet.type = 'text/css';
		document.getElementsByTagName('head')[0].appendChild(stylesheet);
	}
}

// Make slideshow content
( function($) {

		$.fn.make_slideshow = function(ww, hh) {
			this.each(function() {
				var e = $(this);
				e.css({
					width : ww,
					height : hh,
					overflow : 'hidden'
				});
				e.find('> div').css({
					position : 'absolute',
					opacity : 0.0,
					width : ww,
					height : hh,
				});
				var cnt = e.find('> div').length;
				var num = 0;
				e.find('> div:first-child').css({
					zIndex : 10,
					opacity : 1.0
				});
				if (cnt > 1) {
					var t = setInterval(function() {
						var $active = e.find('> div:eq(' + num + ')');
						var ww = 0;
						//$active.width();
						$active.animate({
							left : -ww,
							opacity : 0.0
						}, 1000, function() {
							$active.css({
								zIndex : 9
							});
						});
						num++;
						if (num > cnt - 1)
							num = 0;
						var $next = e.find('> div:eq(' + num + ')');
						var ww = $next.width();
						$next.css({
							left : ww,
							zIndex : 10,
							opacity : 0.0
						}).animate({
							left : 0.0,
							opacity : 1.0
						}, 1000, function() {
							$active.css({
								opacity : 0.0
							});
						});
					}, 6000);
				}
			});

			return this;
		};

		$.fn.popup_slideshow = function() {
			this.each(function() {
				var e = $(this);
				var items = [];
				
				var images = e.data('images');
				if (typeof images == "object") 
					items = images;
					
				var trailer = e.data('trailer');
				if (typeof trailer == "object" && trailer.src.length > 0)
					items.push(trailer);
					
				if (items.length > 0) {
					e.magnificPopup({
						items : items,
						preload : [0, 2],
						gallery : {
							enabled : true,
							cursor : 'mfp-zoom',
							titleSrc : 'title',
							verticalFit : true,
							tCounter : '%curr% / %total%',
							tPrev : 'Hình trước',
							tNext : 'Hình sau',
						},
						image : {
							enabled : true,
							markup : '<div class="mfp-figure"><div class="mfp-close"></div><div class="mfp-img"></div><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></div>',
							tError : 'Không thể tải <a href="%url%" target="new">hình hoặc clip này</a>.',
						},
						type : 'image',
						removalDelay : 300,
						mainClass : 'mfp-newspaper',
						tLoading : 'Chờ xíu, đang lấy hình về...',
					});
				}
			});

			return this;
		};

	}(jQuery));
