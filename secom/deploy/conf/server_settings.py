
DEBUG = {{ DEBUG }}

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ DB_NAME }}',
        'USER': '{{ DB_USER }}',
        'PASSWORD': '{{ DB_PASSWORD }}',
        'HOST': '',
        'PORT': '',
    }
}

# https://docs.djangoproject.com/en/1.6/ref/settings/#std:setting-MEDIA_ROOT
MEDIA_ROOT = '{{ MEDIA_PATH }}'
CKEDITOR_UPLOAD_PATH = '{{ MEDIA_PATH }}'

# https://docs.djangoproject.com/en/1.6/howto/static-files/deployment/
STATIC_ROOT = '{{ STATIC_PATH }}'

HOST = "{{ HOST }}"

# http://django-compressor.readthedocs.org/en/latest/settings/
COMPRESS_ROOT = '{{ STATIC_PATH }}'

