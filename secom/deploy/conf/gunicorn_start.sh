#!/bin/bash
 
NAME="{{ INSTANCE_NAME }}"                           # Name of the application
DJANGODIR={{ APP_PATH }}/{{ APP_NAME }}           # Django project directory
SOCKFILE={{ APP_PATH }}/run/gunicorn.sock  		  # we will communicte using this unix socket
USER={{ APP_USER }}                               # the user to run as
# GROUP=webapps                                   # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE={{ APP_NAME }}.settings    # which settings file should Django use
DJANGO_WSGI_MODULE={{ APP_NAME }}.wsgi            # WSGI module name
 
echo "Starting $NAME as `whoami`"
 
# Activate the virtual environment
cd $DJANGODIR
source {{ APP_PATH }}/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
 
# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
 
exec {{ APP_PATH }}/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER \
  --log-level=debug \
  --bind=unix:$SOCKFILE