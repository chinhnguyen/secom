from __future__ import with_statement
from fabric.api import run, env, cd, prefix

def pip(commands=''):
    """ Runs pip command """
    run('pip ' + commands)
 
def pip_install(what=None, options='', restart=True):
    what = what or env.PIP_CONF
    with cd(env.CONFIG):
        with prefix('source bin/activate'):
            run('pip install %s -r %s' % (options, what))
            run('pip install --no-input setproctitle')


def pip_update(what=None, options='', restart=True):
    what = what or env.PIP_CONF
    with cd(env.CONFIG):
        with prefix('source bin/activate'):
            run('pip install %s -U -r %s' % (options, what)) 

def virtualenv_create():    
    run('mkdir -p %s' % env.CONFIG,)
    run('chown %s %s' % (env.APP_USER, env.CONFIG))
    with cd(env.CONFIG):
        run('virtualenv .')
        
    
