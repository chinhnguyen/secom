'''
Created on Jun 10, 2014

@author: ntr9h
'''
from __future__ import with_statement
from fabric.api import env, abort, shell_env, run
from fabric.context_managers import settings
# from fabric.utils import puts
from .utils import run_as_sudo

postgres_CREATE_USER = """CREATE USER %(db_user)s WITH PASSWORD '%(db_password)s';"""
postgres_CREATE_DB = """CREATE DATABASE %(db_name)s OWNER %(db_user)s"""

def _credentials(db_name=None, db_user=None, db_password=None):
    db_name = db_name or env.DB_NAME
    db_user = db_user or env.DB_USER
    if db_password is None:
        db_password = env.DB_PASSWORD
    return db_name, db_user, db_password

@run_as_sudo
def postgres_execute(sql, user="postgres", password="1Bisnothard"):
    """ Executes passed sql command using mysql shell. """
    sql = sql.replace('"', r'\"')
#     sudo("su - postgres -c 'psql -c \"%s\"'" % sql)
    with shell_env(PGPASSWORD=password):    
        return run('psql -h localhost -U %s -c "%s"' % (user, sql))

def postgres_create_user(db_user=None, db_password=None):
    """ Creates mysql user. """
    _, db_user, db_password = _credentials(None, db_user, db_password)

    if db_user == 'postgres':  # do we really need this?
        abort('Postgresql postgres user can not be created')
        return

    sql = postgres_CREATE_USER % dict(db_user=db_user, db_password=db_password)
    postgres_execute(sql)

def postgres_create_db(db_name=None, db_user=None, root_password=None):
    """ Creates an empty postgres database. """
    db_name, db_user, _ = _credentials(db_name, db_user, None)

    with settings(warn_only=True):
        postgres_create_user(db_user=db_user)
        
        sql = postgres_CREATE_DB % dict(db_name=db_name, db_user=db_user)
        postgres_execute(sql)
