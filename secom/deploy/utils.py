'''
Created on Jun 11, 2014

@author: ntr9h
'''
import os, posixpath
from functools import wraps
from fabric.api import env, sudo, prefix, cd, run
# from fabric.utils import puts
from fabric import network

def run_as_sudo(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        env.host_string = network.join_host_strings(env.SUDO_USER, env.HOST, 22)
        result = func(*args, **kwargs)
        env.host_string = network.join_host_strings(env.APP_USER, env.HOST, 22)
        return result
    return wrapper

def run_as(func, user=None):
    @wraps(func)
    def wrapper(*args, **kwargs):
        user = user or env.APP_USER
        env.host_string = network.join_host_strings(user, env.HOST, 22)
        result = func(*args, **kwargs)
        env.host_string = network.join_host_strings(env.APP_USER, env.HOST, 22)
        return result
    return wrapper


def aptitude_install(packages, options=''):
    sudo('aptitude install %s -y %s' % (options, packages,))
    
def npm_install(packages, options=''):
    sudo('npm install %s -y %s' % (options, packages,))
    
def get_home_dir(username):
    return '/home/%s/' % username

def virtualenv():
    return prefix('source %s/bin/activate' % env.APP_PATH)

def inside_virtualenv(func):
    @wraps(func)
    def inner(*args, **kwargs):
        with virtualenv():
            return func(*args, **kwargs)
    return inner

def inside_src(func):
    def inner(*args, **kwargs):
        with cd(env.SRC_DIR):
            with virtualenv():
                return func(*args, **kwargs)
    return inner

def inside_project(func):
    @wraps(func)
    def inner(*args, **kwargs):
        with cd(posixpath.join(env.APP_PATH, env.APP_NAME)):
            with virtualenv():
                return func(*args, **kwargs)
    return inner

def project_path(name):
    return os.path.join(env.PROJECT_PATH, name)

def remote_project_path(name):
    return posixpath.join(env.APP_PATH, name)

@inside_project
def delete_pyc():
    run("find . -name '*.pyc' -delete")


