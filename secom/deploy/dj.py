'''
Created on Jun 13, 2014

@author: ntr9h
'''
# coding: utf-8
from __future__ import with_statement

import posixpath

from fabric.api import run, settings, env
from fabric.contrib import files

from . import utils

@utils.inside_project
def manage(command):
    run('python manage.py ' + command)

def migrate(params='', do_backup=True):
    manage('migrate --noinput %s' % params)

def syncdb(params=''):
    manage('syncdb --noinput %s' % params)

def compress(params=''):
    with settings(warn_only=True):
        manage('synccompress %s' % params)

def collectstatic(params=''):
    manage('collectstatic --noinput %s' % params)
    
def create_superuser(username=None, email=None):
    username = username or env.DJ_ADMIN_USERNAME
    email = email or env.DJ_ADMIN_EMAIL
    manage('createsuperuser --username %s --email %s' % (username, email))

def load_fixtures():
    manage('loaddata %s' % ' '.join(env.DJ_FIXTURES))
    
def update_settings():
    # copy local's config.server.py to config.py
    files.upload_template(
        env.REMOTE_SETTINGS_TEMPLATE,
        posixpath.join(env.APP_PATH, '%s/%s/config.py' % (env.APP_NAME, env.APP_NAME)),
        env, True)
        

