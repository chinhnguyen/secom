'''
Created on Jun 11, 2014

@author: ntr9h
'''
import os
from fabric.api import env, sudo, cd
from fabric.context_managers import settings
from fabric.contrib import files
from .utils import run_as_sudo, get_home_dir

@run_as_sudo
def create_system_account(pub_key_file, username=None):
    with open(os.path.normpath(pub_key_file), 'rt') as f:
        ssh_key = f.read()

    username = username or env.APP_USER

    with (settings(warn_only=True)):
        sudo('adduser %s --disabled-password --gecos ""' % username)

    home_dir = get_home_dir(username)
    with cd(home_dir):
        sudo('mkdir -p .ssh')
        files.append('.ssh/authorized_keys', ssh_key, use_sudo=True)
        sudo('chown -R %s:%s .ssh' % (username, username))
