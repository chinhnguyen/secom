'''
Created on Jun 11, 2014

@author: ntr9h
'''
import posixpath
from fabric.api import env, sudo, run, cd
from fabric import network
from fabric.context_managers import settings
from fabric.contrib import files
from fabric.utils import puts

from .postgres import postgres_create_db
from .virtualenv import virtualenv_create, pip_install, pip_update
from . import git, utils, dj

# http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/

def task(**kwargs):
    command = kwargs.get('command', '')
    if 'full_deploy' == command:
        tasks = [
            'create_db',
            'update_nginx',
            'create_venv',
            'update_source',
            'pip_install',
            'create_extra_folders',
            'update_dj_settings',
            'syncdb',
            'create_dj_superuser',
            'update_bower',
            'collect_static',
            'restart_nginx',
            'update_gunicorn',
            'supervise_gunicorn',
            'load_dj_fixtures']
    elif 'update' == command:
        tasks = [
            'update_source',
            'pip_update',
            'syncdb',
            'update_bower',
            'collect_static',
            'restart_gunicorn',
            'restart_celery']
    elif 'quick_deploy' == command:
        if kwargs.get('update_pip', '') == 'True':
            tasks = [
                'update_source',
                'pip_update',
                'collect_static',
                'restart_gunicorn',
                'restart_celery']
        else: 
            tasks = [
                'update_source',
                'collect_static',
                'restart_gunicorn',
                'restart_celery']            
    elif 'update_nginx' == command:
        tasks = [
            'update_nginx',
            'restart_nginx']
    else:
        tasks = [command]
        
    # Update system
    if 'prepare_server' in tasks:
        prepare_server()
         
    if 'create_db' in tasks:        
        postgres_create_db()
   
    if 'update_nginx' in tasks:
        update_nginx()
 
    if 'create_venv' in tasks:
        virtualenv_create()
     
    if 'create_extra_folders' in tasks:
        create_folders()
         
    if 'update_gunicorn' in tasks:
        update_gunicorn()
 
    if 'update_source' in tasks:
        update_source()
     
    if 'update_dj_settings' in tasks:    
        dj.update_settings()
         
    if 'pip_install' in tasks:
        pip_install()
     
    if 'pip_update' in tasks:
        pip_update()
     
    if 'syncdb' in tasks:        
        dj.syncdb()
 
    if 'create_dj_superuser' in tasks:        
        dj.create_superuser()
         
    if 'load_dj_fixtures' in tasks:
        dj.load_fixtures()
 
    if 'update_bower' in tasks:
        with cd(posixpath.join(env.APP_PATH, env.APP_NAME)):
            run('bower update')
 
    if 'collect_static' in tasks:
        dj.collectstatic()
         
    if 'supervise_gunicorn' in tasks:
        supervise_gunicorn()
         
    if 'restart_nginx' in tasks:
        nginx_restart()
         
    if 'restart_gunicorn' in tasks:
        restart_gunicorn()
 
    if 'restart_celery' in tasks:
        restart_celery()
        
    if 'dj_manage' in tasks:
        dj_command = kwargs.get('dj_command', '')
        if len(dj_command) > 0:
            dj.manage(dj_command)
        
         
def update_source():
    with cd(env.CONFIG):
        if not files.exists(".git"):
            with settings(warn_only=True):
                git.init()
    # push code to server    
    git.local_push()
    # delete pyc (might fail the first time)
    with settings(warn_only=True):
        utils.delete_pyc()
    # checkout the new code
    with cd(env.CONFIG):
        git.up()
   
def update_gunicorn():     
    files.upload_template(
        env.GUNICORN_BASH_TEMPLATE,
         '%s/bin' % env.APP_PATH,
         env, True)
    set_executable('%s/bin/gunicorn_start.sh' % env.APP_PATH)


@utils.run_as_sudo    
def supervise_gunicorn():
    files.upload_template(
        env.SUPERVISOR_CONF_TEMPLATE,
         '/etc/supervisor/conf.d/%s.conf' % env.INSTANCE_NAME,
         env, True, use_sudo=True)
    # store application log message
    run('touch %s/gunicorn_supervisor.log' % env.LOGS_PATH)
    sudo('supervisorctl reread')
    sudo('supervisorctl update')
    sudo('supervisorctl status %s' % env.INSTANCE_NAME)
    
@utils.run_as_sudo    
def restart_gunicorn():
    sudo('supervisorctl restart %s' % env.INSTANCE_NAME)

@utils.run_as_sudo    
def restart_celery():
    sudo('supervisorctl restart %s_celery' % env.INSTANCE_NAME)

@utils.run_as_sudo
def set_executable(filename):
    sudo('chmod u+x %s' % filename)
        
def create_folders():
    # create folder for static files and uploaded files
    run('mkdir -p %s' % env.STATIC_PATH)
    run('mkdir -p %s' % env.MEDIA_PATH)
    run('mkdir -p %s' % env.LOGS_PATH)
    run('mkdir -p %s' % posixpath.join(env.APP_PATH, 'run'))
    

@utils.run_as_sudo
def update_nginx():
    files.upload_template(
        env.NGINX_CONFIG_TEMPLATE,
         '/etc/nginx/sites-available/%s' % env.INSTANCE_NAME,
         env, True)
    with settings(warn_only=True):
        sudo('ln -sf /etc/nginx/sites-available/%s /etc/nginx/sites-enabled/%s' % (env.INSTANCE_NAME, env.INSTANCE_NAME))
    
    
@utils.run_as_sudo
def nginx_restart():
    sudo('invoke-rc.d nginx restart')
    

@utils.run_as_sudo
def prepare_server():
    # Update system
    sudo('aptitude update')
    sudo('aptitude upgrade -y')
    # for command add-apt-repository
    # see http://stackoverflow.com/questions/13018626/add-apt-repository-not-found
    utils.aptitude_install('software-properties-common')
    utils.aptitude_install('python-software-properties')
    # for nodejs 
    sudo('add-apt-repository ppa:chris-lea/node.js -y')    
    sudo('aptitude update')
    # install dependencies
    utils.aptitude_install('nodejs')
    utils.aptitude_install('git')
    utils.aptitude_install('python-virtualenv python-dev')
    utils.aptitude_install('postgresql postgresql-contrib libpq-dev')
    utils.aptitude_install('supervisor')
    utils.aptitude_install('nginx')
    sudo('rm -f /etc/nginx/sites-enabled/default')
    
    # install bower
    sudo('npm install -g bower')
    
    # install less
    sudo('npm install -g less')
