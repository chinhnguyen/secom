from fabric.api import run, local, env

BRANCH_OPTION = 'GIT_BRANCH'

def init():
    run('git init')
    run('git config receive.denyCurrentBranch ignore')  # allow update current branch

def up():
    run('git checkout --force %s' % env.GIT_BRANCH)  # overwrite local changes

def local_push():
    local('git push --force ssh://%s@%s/~/%s/ %s' % (env.APP_USER, env.HOST, env.CONFIG, env.GIT_BRANCH))

